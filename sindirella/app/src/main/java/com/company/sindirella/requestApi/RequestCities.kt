package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Category.CategoriesIdResponse
import com.company.sindirella.response.City.CitiesResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestCities(activity: AppCompatActivity?, fragment: Fragment?,orderSort: String?,orderName: String?,page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.Cities>(Service.Cities::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCities(orderSort,orderName,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}