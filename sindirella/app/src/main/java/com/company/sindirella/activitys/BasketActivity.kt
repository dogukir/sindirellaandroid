package com.company.sindirella.activitys

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.OrderDetailController
import com.company.sindirella.helpers.BasketRecycleAdapter
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.OrderDetail.GetOrderDetailResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class BasketActivity : AppCompatActivity(),OrderDetailController.OrderDetailsListener,BasketRecycleAdapter.DeleteProductListener {


    private var recycleBasket : RecyclerView? = null
    private var adapter : BasketRecycleAdapter? = null
    private var complateShoppingButton : Button? = null
    private var backPressedImage : ImageView? = null


    private var orderDetailController: OrderDetailController? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basket)
        supportActionBar?.hide()

        orderDetailController = OrderDetailController(this,null)
        orderDetailController?.orderDetailsListener = this
        orderDetailController?.getOrderDetails(appPreferences.token,null,null,0,null,"DESC",null)


        var arr = ArrayList<String>()

        arr.add("dogukan")
        arr.add("dogu")
        arr.add("do")
        arr.add("dogukan")
        arr.add("doguka")
        arr.add("doguk")


        recycleBasket = findViewById(R.id.recycleBasket)
        complateShoppingButton = findViewById(R.id.complateShoppingButton)
        backPressedImage = findViewById(R.id.backPressedImage)



        val layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        recycleBasket?.layoutManager = layoutManager
        recycleBasket?.itemAnimator = DefaultItemAnimator()





        backPressedImage?.setOnClickListener {

            onBackPressed()

        }

        complateShoppingButton?.setOnClickListener {

            if (recycleBasket?.size!! > 0){

                val intent = Intent(applicationContext,PayActivity::class.java)
                startActivity(intent)

            }else{

                Toast.makeText(applicationContext,"Sepetinizde Ürün Bulunmamaktadır.",Toast.LENGTH_LONG).show()

            }

        }


        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        complateShoppingButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.sindirella_blue)).cornerRadius(20).build()


    }

    override fun getOrderDetails(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var orderDetailData = Gson().fromJson(jsonElement,GetOrderDetailResponse::class.java)

            if (!orderDetailData.hydraMember.isNullOrEmpty()){

                adapter = BasketRecycleAdapter(this,orderDetailData.hydraMember!!)
                adapter?.deleteProductListener = this
                recycleBasket?.adapter = adapter

            }else{

                Toast.makeText(applicationContext,"Sepetinizde Ürün Bulunmamaktadır.",Toast.LENGTH_LONG).show()

            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun deleteProduct(id: Int?) {

        orderDetailController?.deleteOrderDetailId(appPreferences.token,id.toString())

    }

    override fun postCheckOrder(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

    }

    override fun postOrderDetails(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {

    }

    override fun deleteOrderDetailsId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            println("doğru")

        }else{

            println("yanlış")

        }

        orderDetailController?.getOrderDetails(appPreferences.token,null,null,1,null,null,null)

    }




}