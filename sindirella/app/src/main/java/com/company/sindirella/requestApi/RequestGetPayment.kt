package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.OrderShipmentAddress.OrderShipmentAddressIdResponse
import com.company.sindirella.response.Payment.GetPaymentResponse
import com.google.gson.JsonArray
import java.util.ArrayList

class RequestGetPayment(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<JsonArray>) {

    init {
        val request = RequestCreator.create<Service.GetPayment>(Service.GetPayment::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getPayment(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}