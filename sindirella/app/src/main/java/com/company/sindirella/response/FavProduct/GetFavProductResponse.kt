package com.company.sindirella.response.FavProduct

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class GetFavProductResponse {

    @SerializedName("hydra:member")
    var hydraMember : ArrayList<GetFavProductResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems : Int? = null

}

class GetFavProductResponseHydraMember {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("product")
    var product: GetFavProductResponseHydraMemberProduct? = null

    init {
        id = null
        product = null
    }

}

class GetFavProductResponseHydraMemberProduct {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    init {
        id = null
    }

}