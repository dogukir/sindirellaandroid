package com.company.sindirella.helpers

object GlobalData {

    var base_url = "https://api.sindirella.com/api/"
    var dynamic_url = "https://api.sindirella.com/api/"
    var browse_clothes = ""
    var promotionAreaClickUrl : String? = null

    var productDetailActivitiesId = -1
    var loginEmail = ""
    var showButtonClick = false



    var selectedMinAmount: String? = null
    var selectedMaxAmount: String? = null
    var selectedCategoryId: Int? = null
    var selectedSizeId: ArrayList<String>? = null
    var selectedColorId: ArrayList<String>? = null
    var selectedBrandId: Int? = null
    var selectedStartDate: String? = null
    var selectedEndDate: String? = null
    var selectedEventsId: ArrayList<String>? = null

    var filterClick: Boolean? = null

}