package com.company.sindirella.response.Country

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class CountriesResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<CountriesResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class CountriesResponseHydraMember{

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("code")
    var code: String? = null

    init {
        id = null
        name = null
        code = null
    }

}