package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import com.company.sindirella.response.Color.ColorsResponseHydraMember
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class FilterColorBaseAdapter(var context: Context,var colorData: ArrayList<ColorsResponseHydraMember>) : BaseAdapter() {

    private var colorLayout: RelativeLayout? = null
    var selectedArray = ArrayList<Int>()
    var sendSelectedColorListener: SendSelectedColorListener? = null


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.filter_color_grid_view,parent,false)

        colorLayout = view.findViewById(R.id.colorLayout)

        if (colorData.get(position).hex?.length!! > 4){


            if (selectedArray.isEmpty()){

                colorLayout?.background = DrawableBuilder().solidColor(Color.parseColor("#${colorData.get(position).hex}")).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(10).build()

            }else{

                var control = false

                for (i in 0 until selectedArray.size){

                    if (selectedArray.get(i) == position){

                        control = true
                        colorLayout?.background = DrawableBuilder().solidColor(Color.parseColor("#${colorData.get(position).hex}")).strokeColor(context.resources.getColor(R.color.sindirella_blue)).strokeWidth(5).cornerRadius(10).build()

                    }

                }

                if (!control){

                    colorLayout?.background = DrawableBuilder().solidColor(Color.parseColor("#${colorData.get(position).hex}")).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(10).build()

                }

            }

        }else{

            if (selectedArray.isEmpty()){

                colorLayout?.background = DrawableBuilder().solidColor(context.resources.getColor(R.color.sindirella_blue)).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(10).build()

            }else{

                var control = false

                for (i in 0 until selectedArray.size){

                    if (selectedArray.get(i) == position){

                        control = true
                        colorLayout?.background = DrawableBuilder().solidColor(context.resources.getColor(R.color.sindirella_blue)).strokeColor(context.resources.getColor(R.color.sindirella_blue)).strokeWidth(5).cornerRadius(10).build()

                    }

                }

                if (!control){

                    colorLayout?.background = DrawableBuilder().solidColor(context.resources.getColor(R.color.sindirella_blue)).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(10).build()

                }

            }

        }


        colorLayout?.setOnClickListener {

            if (selectedArray.isEmpty()){

                selectedArray.add(position)

            }else{

                var control = false

                for (i in selectedArray.size-1 downTo 0 step 1){

                    if (selectedArray.get(i) == position){

                        selectedArray.removeAt(i)
                        control = true

                    }

                }

                if (!control){
                    selectedArray.add(position)
                }

            }

            sendSelectedColorListener?.selectedColorListener(selectedArray)

            notifyDataSetChanged()

        }




        return view

    }

    override fun getItem(position: Int): Any {
        return colorData.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return colorData.size
    }

    interface SendSelectedColorListener{
        fun selectedColorListener(selectedArray: ArrayList<Int>)
    }

}