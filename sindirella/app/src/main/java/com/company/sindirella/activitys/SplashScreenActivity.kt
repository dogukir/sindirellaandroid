package com.company.sindirella.activitys

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.company.sindirella.R
import com.company.sindirella.helpers.GlobalData

class SplashScreenActivity : AppCompatActivity() {

    var splashLayout : ConstraintLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        supportActionBar?.hide()

        GlobalData.filterClick = false

        splashLayout = findViewById(R.id.splashLayout)


        /*
        Handler().postDelayed({
            val intent = Intent(this, HowWorkingActivity::class.java)
            startActivity(intent)
            finish()
        }, 1500)

         */

        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 1500)



        setUI()

    }



    fun setUI(){

        splashLayout?.setBackgroundColor(resources.getColor(R.color.sindirella_blue))

        window.statusBarColor = resources.getColor(R.color.sindirella_blue)

    }

}