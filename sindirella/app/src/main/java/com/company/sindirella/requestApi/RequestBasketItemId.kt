package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Activity.ActivitysIdResponse
import com.company.sindirella.response.BasketItem.BasketItemsIdResponse

class RequestBasketItemId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<BasketItemsIdResponse>) {

    init {
        val request = RequestCreator.create<Service.BasketItemId>(Service.BasketItemId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getBasketItemId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}