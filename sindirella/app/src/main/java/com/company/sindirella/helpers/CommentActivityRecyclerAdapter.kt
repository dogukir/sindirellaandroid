package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.response.Comment.GetCommentsResponseHydraMembers
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class CommentActivityRecyclerAdapter(private val context: Context?, private var data : ArrayList<GetCommentsResponseHydraMembers>): RecyclerView.Adapter<CommentActivityRecyclerAdapter.ViewHolder>() {

    private var tabLayout: LinearLayout? = null
    private var textViewName: TextView? = null
    private var commentDetailIcon: ImageView? = null
    private var dateText: TextView? = null
    private var titleProductText: TextView? = null
    private var productDetailText: TextView? = null
    private var profilePhoto: ImageView? = null
    private var buttonComment: Button? = null
    private var detailLayout: LinearLayout? = null
    private var commentRatingStar: RatingBar? = null

    var commentClickListener: CommentClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.comment_recycler_item_view_two,parent,false)

        tabLayout = view.findViewById(R.id.tabLayout)
        textViewName = view.findViewById(R.id.textViewName)
        commentDetailIcon = view.findViewById(R.id.commentDetailIcon)
        dateText = view.findViewById(R.id.dateText)
        titleProductText = view.findViewById(R.id.titleProductText)
        productDetailText = view.findViewById(R.id.productDetailText)
        profilePhoto = view.findViewById(R.id.profilePhoto)
        buttonComment = view.findViewById(R.id.buttonComment)
        detailLayout = view.findViewById(R.id.detailLayout)
        commentRatingStar = view.findViewById(R.id.commentRatingStar)




        tabLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.how_working_status_bar)).cornerRadii(20,20,0,0).build()
        detailLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadii(0,0,20,20).build()
        buttonComment?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.transparent)).strokeColor(ContextCompat.getColor(context!!,R.color.black)).strokeWidth(3).cornerRadius(20).build()

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        textViewName?.text = data.get(position).client?.name
        dateText?.text = data.get(position).formattedDate
        titleProductText?.text = data.get(position).title
        productDetailText?.text = data.get(position).body
        commentRatingStar?.rating = data.get(position).rating!!.toFloat()

        if (!data.get(position).medias.isNullOrEmpty()){

            val requestOptions = RequestOptions()
            requestOptions.transform(CenterCrop(), GranularRoundedCorners(20f,20f,20f,20f))

            Glide.with(context!!).load(data.get(position).medias?.get(0)?.imageUrl).apply(requestOptions).into(profilePhoto!!)
        }

        buttonComment?.setOnClickListener {

            commentClickListener?.commentClick(null)

        }

        if (data.get(position).medias.isNullOrEmpty()){

            commentDetailIcon?.visibility = View.GONE
        }

        commentDetailIcon?.setOnClickListener {

            commentClickListener?.commentDetailClick(position)

        }


        holder.setIsRecyclable(false)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

    interface CommentClickListener {
        fun commentClick(id: Int?)
        fun commentDetailClick(position: Int?)
    }

}