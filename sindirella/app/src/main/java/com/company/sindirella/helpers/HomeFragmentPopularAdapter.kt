package com.company.sindirella.helpers

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.response.Product.ProductResponseHydraMember
import org.w3c.dom.Text
import top.defaults.drawabletoolbox.DrawableBuilder

class HomeFragmentPopularAdapter(private val context: Context?,private var productResponse: ArrayList<ProductResponseHydraMember>, var favData: GetFavProductResponse?) : RecyclerView.Adapter<HomeFragmentPopularAdapter.ViewHolder>() {


    /*private var items: ArrayList<FilterUniStudentAllData> = ArrayList()
    private var filteredList: ArrayList<FilterUniStudentAllData> = ArrayList()
    var mSelectedItems: ArrayList<FilterUniStudentAllData> = ArrayList()
    var relativeLayout : RelativeLayout? = null
    var allButtons : ArrayList<CustomCheckBox> = ArrayList()
    var sendDataFilterUni : sendSelectFiltersUni? = null
    var checkBoxControll: ArrayList<Int?>? = null*/

    private var mainLayout: LinearLayout? = null
    private var imageViewClothes: ImageView? = null
    private var imageLike: ImageView? = null
    private var viewLine: View? = null
    private var brandName: TextView? = null
    private var clothesName: TextView? = null
    private var pricesNew: TextView? = null
    private var viewLinePrices: View? = null
    private var pricesOld: TextView? = null
    private var clothesView: RelativeLayout? = null
    var productData : ArrayList<ProductResponseHydraMember>? = ArrayList()

    var popularAdapterClickListener: PopularAdapterClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.mainpage_popular_view, parent, false)


        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var likeDislikeBool = false

        holder.mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()

        var requestOptions = RequestOptions()
        requestOptions.transform(CenterCrop(), GranularRoundedCorners(50f,50f,0f,0f))

        Glide.with(context).load(productData?.get(position)?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(holder.imageViewClothes!!)
        //imageViewClothes?.background = DrawableBuilder().cornerRadii(50,50,0,0).build()
        holder.brandName?.text = productData?.get(position)?.brand?.name
        holder.clothesName?.text = productData?.get(position)?.name
        holder.pricesNew?.text = productData?.get(position)?.priceRental.toString() + " ₺"
        holder.pricesOld?.text = productData?.get(position)?.priceSale.toString() + " ₺"
        holder.pricesOld?.paintFlags = (Paint.STRIKE_THRU_TEXT_FLAG)

        holder.mainLayout?.setOnClickListener {

            popularAdapterClickListener?.popularClick(productData?.get(position)?.BigId)

        }
        
        if (favData != null){

            var favControl = false

            for (i in 0..favData?.hydraMember?.size!!-1){

                if (productData?.get(position)?.id == favData?.hydraMember?.get(i)?.product?.id){

                    favControl = true

                }

            }

            if (favControl == true){

                likeDislikeBool = true
                holder.imageLike?.background = ContextCompat.getDrawable(context,R.drawable.heart_like_otuziki)

            }else{

                holder.imageLike?.background = ContextCompat.getDrawable(context,R.drawable.heart_dislike_otuziki)

            }
            
        }
        

        holder.imageLike?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(context,LoginActivity::class.java)
                context.startActivity(intent)
            }else{

                if (likeDislikeBool == true){

                    for (i in 0..favData?.hydraMember?.size!!-1){

                        if (productData?.get(position)?.id == favData?.hydraMember?.get(i)?.product?.id){

                            popularAdapterClickListener?.disLikeClick(favData?.hydraMember?.get(i)?.id,position)

                        }

                    }

                }else{

                    popularAdapterClickListener?.likeClick(productData?.get(position)?.id,position)

                    likeDislikeBool = true

                }



            }

        }


        //holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {

        return productData?.size!!

    }

    fun add(productVal: ProductResponseHydraMember) {

        /*if (preferencesQuery.id == -1){

        }else{
            items.add(preferencesQuery)
            filteredList.add(preferencesQuery)
            notifyDataSetChanged()
        }*/

        productData?.add(productVal)
        notifyDataSetChanged()

    }

    fun clearItems(){

        notifyDataSetChanged()

    }

    fun addAll(queries: ArrayList<ProductResponseHydraMember>) {

        if (!queries.isNullOrEmpty()) {

            for (query in queries) {

                add(query)

            }

        }

    }

    fun changeFav(favData: GetFavProductResponse, likePosition: Int?){

        this.favData = favData

        //notifyDataSetChanged()
        //notifyDataSetChanged()

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mainLayout = itemView.findViewById<LinearLayout>(R.id.mainLayout)
        var imageViewClothes = itemView.findViewById<ImageView>(R.id.imageViewClothes)
        var imageLike = itemView.findViewById<ImageView>(R.id.imageLike)
        var viewLine = itemView.findViewById<View>(R.id.viewLine)
        var brandName = itemView.findViewById<TextView>(R.id.brandName)
        var clothesName = itemView.findViewById<TextView>(R.id.clothesName)
        var pricesNew = itemView.findViewById<TextView>(R.id.pricesNew)
        var viewLinePrices = itemView.findViewById<View>(R.id.viewLinePrices)
        var pricesOld = itemView.findViewById<TextView>(R.id.pricesOld)
        var clothesView = itemView.findViewById<RelativeLayout>(R.id.clothesView)

        /*mainLayout = v.findViewById(R.id.mainLayout)
        imageViewClothes = v.findViewById(R.id.imageViewClothes)
        imageLike = v.findViewById(R.id.imageLike)
        viewLine = v.findViewById(R.id.viewLine)
        brandName = v.findViewById(R.id.brandName)
        clothesName = v.findViewById(R.id.clothesName)
        pricesNew = v.findViewById(R.id.pricesNew)
        viewLinePrices = v.findViewById(R.id.viewLinePrices)
        pricesOld = v.findViewById(R.id.pricesOld)
        clothesView = v.findViewById(R.id.clothesView)



        //clothesView?.background = DrawableBuilder().cornerRadius(50).build()
        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(50).build()

         */

    }

    interface PopularAdapterClickListener {
        fun popularClick(BigId: String?)
        fun likeClick(id: Int?,position: Int?)
        fun disLikeClick(id: Int?,position: Int?)
    }


}
