package com.company.sindirella.activitys

import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.fragments.HomeFragment
import com.company.sindirella.fragments.MenuFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    var navigationView: BottomNavigationView? = null
    var unselectedColor: Int? = null
    var selectedColor: Int? = null
    var openMenu = false
    var selectedFragment = 1

    override fun onResume() {

        println("dogru")
        super.onResume()
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        selectedColor = resources.getColor(R.color.black)
        unselectedColor = resources.getColor(R.color.grey_color)



        appPreferences.init(applicationContext)

        navigationView = findViewById(R.id.navigationView)

        navigationView?.menu?.add(Menu.NONE, 1, Menu.NONE, resources.getString(R.string.home_fragment))?.setIcon(R.drawable.home_icon)

        navigationView?.menu?.add(Menu.NONE, 2, Menu.NONE, resources.getString(R.string.browse_fragment))?.setIcon(R.drawable.four_otuziki)

        navigationView?.menu?.add(Menu.NONE, 3, Menu.NONE, resources.getString(R.string.basket_fragment))?.setIcon(R.drawable.basket_icon)

        navigationView?.menu?.add(Menu.NONE, 4, Menu.NONE, resources.getString(R.string.menu_fragment))?.setIcon(R.drawable.menu_icon)

        changeMenuColorToColor( navigationView?.menu,  0, 0, resources.getColor(R.color.black))
        changeMenuTestColorToColor(navigationView?.menu,  0, 0, resources.getColor(R.color.black))

        changeFragment(HomeFragment(this))





        navigationView?.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {

                val f = supportFragmentManager.findFragmentById(R.id.container)

                when(item.itemId){

                    1 ->

                        if (f !is HomeFragment) {

                            openMenu = false

                            selectedFragment = 1

                            val homeFragment = HomeFragment(this@MainActivity)
                            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, item.itemId-1)
                            changeFragment(homeFragment)
                            navigationView?.menu?.getItem(0)?.isChecked = true
                        }

                    2 ->

                        if (f !is BrowseFragment) {

                            openMenu = false

                            selectedFragment = 2

                            val browseFragment = BrowseFragment(this@MainActivity)
                            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, item.itemId-1)
                            changeFragment(browseFragment)

                        }

                    3 -> {

                        openMenu = false

                        if (appPreferences.token.equals("")){
                            val intent = Intent(applicationContext,LoginActivity::class.java)
                            startActivity(intent)
                        }else{
                            val intent = Intent(applicationContext, BasketActivity::class.java)
                            startActivity(intent)
                        }

                    }

                    4 ->

                        if (f !is MenuFragment) {

                            if(!openMenu){

                                openMenu = true

                                val menuFragment = MenuFragment()
                                changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, item.itemId-1)
                                changeFragment(menuFragment)

                            }

                        }else{

                            openMenu = false

                            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, selectedFragment-1)

                            onBackPressed()

                        }


                }

                return true
            }

        })






        setUI()

    }


    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    fun changeBottomNaviColor(menu: Menu?, i1: Int, i2: Int, unselectedColor: Int,selectedColor: Int, index: Int) {
        var buttonIndex = index
        val unselectedColor = unselectedColor
        for (i in i1..i2) {

            val drawable = menu?.getItem(i)?.icon
            var spanString = SpannableString(menu?.getItem(i)?.title.toString())
            if (drawable != null && spanString != null) {

                if(buttonIndex== i){
                    drawable.mutate()
                    drawable.setColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP)
                    spanString.setSpan(ForegroundColorSpan(selectedColor), 0, spanString.length, 0)
                    menu.getItem(i).title = spanString
                }
                else {

                    drawable.mutate()
                    drawable.setColorFilter(unselectedColor, PorterDuff.Mode.SRC_ATOP)
                    spanString.setSpan(ForegroundColorSpan(unselectedColor), 0, spanString.length, 0)
                    menu.getItem(i).title = spanString
                }
            }

        }

    }

    fun changeMenuColorToColor(menu: Menu?, i1: Int, i2: Int, color: Int) {
        val unselectedColor = color
        for (i in i1..i2) {

            val drawable = menu?.getItem(i)?.icon
            if (drawable != null) {

                drawable.mutate()
                //drawable.setColorFilter(ContextCompat.getColor(applicationContext!!,R.color.grey_darker), PorterDuff.Mode.SRC_ATOP)
                drawable.setColorFilter(unselectedColor,PorterDuff.Mode.SRC_ATOP)

            }

        }

    }

    fun changeMenuTestColorToColor(menu: Menu?, i1: Int, i2: Int, color: Int) {
        val unselectedColor = color
        for (i in i1..i2) {

            var spanString = SpannableString(menu?.getItem(i)?.title.toString())
            if (spanString != null) {
                spanString.setSpan(ForegroundColorSpan(unselectedColor), 0, spanString.length, 0)
                menu?.getItem(i)?.title = spanString
            }

        }

    }



    fun changeFragment(baseFragment: Fragment) {

        bottomNavLogic(baseFragment)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, baseFragment)
        transaction.addToBackStack(baseFragment.toString())
        transaction.commit()

    }

    fun bottomNavLogic(f: Fragment) {

        if (f is HomeFragment) {

            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, 0)
            navigationView?.menu?.getItem(navigationView?.menu?.size()!!-4)?.isChecked = true

        } else if (f is BrowseFragment) {

            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, 1)
            navigationView?.menu?.getItem(navigationView?.menu?.size()!!-3)?.isChecked = true

        } else if (f is MenuFragment) {

            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, 3)
            navigationView?.menu?.getItem(navigationView?.menu?.size()!!-2)?.isChecked = true

        }

        /*else if (f is CalendarFragment || f is WeeklyScheduleFragment || f is CalendarTeacherFragment) {
            if(bottomNavigationView?.menu?.size()!! > 0)
            {
                bottomNavigationView?.menu?.getItem(bottomNavigationView?.menu?.size()!!-1)?.isChecked = true
            }

        } else if(f is MainPageTeacherFragment || f is GuidanceStudentFragment){

            bottomNavigationView?.menu?.getItem(0)?.isChecked = true

        }else{
            if(bottomNavigationView?.menu?.size()!! > 3)
            {
                bottomNavigationView?.menu?.getItem(bottomNavigationView?.menu?.size()!!-4)?.isChecked = true
            }

        }

         */

    }


    override fun onBackPressed() {

        val f = supportFragmentManager?.findFragmentById(R.id.container)

        if(f is HomeFragment){

            openMenu = false

            super.onBackPressed()



            //this.deActivateNav()
        }else if (f is BrowseFragment) {

            openMenu = false

            selectedFragment = 1

            val homeFragment = HomeFragment(this@MainActivity)
            changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, selectedFragment-1)
            changeFragment(homeFragment)
            navigationView?.menu?.getItem(0)?.isChecked = true

            //super.onBackPressed()

            /*if (!f.testFinished) {

                f.onBackWarning()

            } else {
                if (this is MainPageActivity) {
                    currentActivity?.activateNavigation()
                }
                popPrevFragment()

            }

             */

        } else if (f is MenuFragment) {

            if (selectedFragment == 1){

                openMenu = false

                selectedFragment = 1

                val homeFragment = HomeFragment(this@MainActivity)
                changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, selectedFragment-1)
                changeFragment(homeFragment)
                navigationView?.menu?.getItem(0)?.isChecked = true

            }else{

                openMenu = false

                selectedFragment = 2

                val browseFragment = BrowseFragment(this@MainActivity)
                changeBottomNaviColor(navigationView?.menu, 0, navigationView?.menu?.size()!!-1, unselectedColor!!, selectedColor!!, selectedFragment-1)
                changeFragment(browseFragment)

            }

            //popPrevFragment()

        } else {

            super.onBackPressed()
            /*if (this is LoginActivity){

            }else{
                popPrevFragment()

            }*/
            //popPrevFragment()

        }


    }

    override fun onDestroy() {

        //appPreferences.token = ""

        super.onDestroy()

    }

    override fun onStop() {

        //appPreferences.token = ""
        super.onStop()
    }


}
