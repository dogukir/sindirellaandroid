package com.company.sindirella.response.Activity

import com.google.gson.annotations.SerializedName

class ActivitysIdResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("imageCode")
    var imageCode : String? = null

    init {

        id = null
        name = null
        imageCode = null

    }

}