package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestBrands
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class BrandController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var brandsListener: BrandsListener? = null

    fun brands(page: Int?){
        RequestBrands(activity,fragment,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                brandsListener?.getBrands(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                brandsListener?.getBrands(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                brandsListener?.getBrands(false,null,failMessage,error)
            }

        })
    }


    interface BrandsListener{
        fun getBrands(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}