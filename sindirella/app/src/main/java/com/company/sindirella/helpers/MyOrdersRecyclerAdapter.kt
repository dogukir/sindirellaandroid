package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.response.Comment.GetCommentsResponseHydraMembers
import com.company.sindirella.response.Order.ActiveOrdersResponse
import com.company.sindirella.response.Order.ActiveOrdersResponseHydraMember
import com.company.sindirella.response.Order.ActiveOrdersResponseHydraMemberOrderDetail
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class MyOrdersRecyclerAdapter(private val context: Context?, private var data : ArrayList<ActiveOrdersResponseHydraMemberOrderDetail>, private var mainData: ActiveOrdersResponse): RecyclerView.Adapter<MyOrdersRecyclerAdapter.ViewHolder>() {


    private var productImage: ImageView? = null
    private var transactionIdText: TextView? = null
    private var dateText: TextView? = null
    private var payConfirmText: TextView? = null
    private var totalAmountText: TextView? = null
    private var paymentStrText: TextView? = null
    private var mainLayout: LinearLayout? = null
    private var rightLayout: LinearLayout? = null

    var myOrderClickListener: MyOrderClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.my_orders_recycler_item_view,parent,false)

        productImage = view.findViewById(R.id.productImage)
        transactionIdText = view.findViewById(R.id.transactionIdText)
        dateText = view.findViewById(R.id.dateText)
        payConfirmText = view.findViewById(R.id.payConfirmText)
        totalAmountText = view.findViewById(R.id.totalAmountText)
        paymentStrText = view.findViewById(R.id.paymentStrText)
        mainLayout = view.findViewById(R.id.mainLayout)
        rightLayout = view.findViewById(R.id.rightLayout)



        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mainData.hydraMember?.size!!
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (!mainData.hydraMember?.get(position)?.orderDetails.isNullOrEmpty()){

            var requestOptions = RequestOptions()
            requestOptions.transform(CenterCrop(), GranularRoundedCorners(40f,0f,0f,40f))

            Glide.with(context!!).load(mainData.hydraMember?.get(position)?.orderDetails?.get(0)?.product?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(productImage!!)

        }

        if (!mainData.hydraMember.isNullOrEmpty()){

            transactionIdText?.text = "#${mainData.hydraMember?.get(0)?.transactionId}"
            dateText?.text = mainData.hydraMember?.get(position)?.orderedAtStr
            payConfirmText?.text = mainData.hydraMember?.get(position)?.statusStr
            totalAmountText?.text = String.format("%.1f", mainData.hydraMember?.get(position)?.amountTotal!!.toFloat()) + " ₺"
            paymentStrText?.text = mainData.hydraMember?.get(position)?.paymentType

        }

        mainLayout?.setOnClickListener {

            myOrderClickListener?.myOrderClick(mainData.hydraMember?.get(position)?.id)

        }

        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(40).build()
        rightLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadii(0,40,40,0).build()



        holder.setIsRecyclable(false)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

    interface MyOrderClickListener {
        fun myOrderClick(id: Int?)
    }

}