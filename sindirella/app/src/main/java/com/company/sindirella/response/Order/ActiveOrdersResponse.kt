package com.company.sindirella.response.Order

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ActiveOrdersResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<ActiveOrdersResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class ActiveOrdersResponseHydraMember {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("orderedAt")
    var orderedAt : String? = null

    @SerializedName("amountNet")
    var amountNet : Int? = null

    @SerializedName("amountVat")
    var amountVat : Int? = null

    @SerializedName("amountShipment")
    var amountShipment : Int? = null

    @SerializedName("amountTotal")
    var amountTotal : Int? = null

    @SerializedName("orderDetails")
    var orderDetails : ArrayList<ActiveOrdersResponseHydraMemberOrderDetail>? = null

    @SerializedName("payment")
    var payment : Payment? = null

    @SerializedName("shipmentAddress")
    var shipmentAddress : ActiveOrdersResponseHydraMemberShipmentAddress? = null

    @SerializedName("invoiceAddress")
    var invoiceAddress : ActiveOrdersResponseHydraMemberInvoiceAddress? = null

    @SerializedName("status")
    var status : Int? = null

    @SerializedName("statusStr")
    var statusStr : String? = null

    @SerializedName("orderedAtStr")
    var orderedAtStr: String? = null

    @SerializedName("transactionId")
    var transactionId : String? = null

    @SerializedName("cargoId")
    var cargoId : Int? = null

    @SerializedName("paymentType")
    var paymentType : String? = null

    init {
        id = null
        orderedAt = null
        amountNet = null
        amountVat = null
        amountShipment = null
        amountTotal = null
        orderDetails = null
        payment = null
        shipmentAddress = null
        invoiceAddress = null
        status = null
        statusStr = null
        transactionId = null
    }

}



class ActiveOrdersResponseHydraMemberOrderDetail {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("invertory")
    var invertory : String? = null

    @SerializedName("toDate")
    var toDate : String? = null

    @SerializedName("basket")
    var basket : String? = null

    @SerializedName("days")
    var days : Int? = null

    @SerializedName("product")
    var product : ActiveOrdersResponseHydraMemberOrderDetailProduct? = null

    init {
        id = null
        invertory = null
        toDate = null
        basket = null
        days = null
        product = null
    }

}


class ActiveOrdersResponseHydraMemberOrderDetailProduct {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("description")
    var description : String? = null

    @SerializedName("notes")
    var note : String? = null

    @SerializedName("brand")
    var brand : ActiveOrdersResponseHydraMemberOrderDetailProductBrand? = null

    @SerializedName("color")
    var color : String? = null

    @SerializedName("isRental")
    var isRental : Boolean? = null

    @SerializedName("pictures")
    var pictures : ArrayList<ActiveOrdersResponseHydraMemberOrderDetailProductPictures>? = null

}

class ActiveOrdersResponseHydraMemberOrderDetailProductPictures {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("imageUrl")
    var imageUrl : String? = null

    @SerializedName("originSizeUrl")
    var originSizeUrl : String? = null

    @SerializedName("showOrder")
    var showOrder : Int? = null

}

class ActiveOrdersResponseHydraMemberOrderDetailProductBrand {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("name")
    var name : String? = null

}


class Payment {

    @SerializedName("typeStr")
    var typeStr : String? = null

    init {
        typeStr = null
    }

}

class ActiveOrdersResponseHydraMemberShipmentAddress {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("address")
    var address : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("county")
    var county : String? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("zipCode")
    var zipCode : String? = null

    @SerializedName("district")
    var district : String? = null

    @SerializedName("receiverName")
    var receiverName : String? = null

    @SerializedName("phone")
    var phone : String? = null

    init {
        name = null
        address = null
        country = null
        county = null
        city = null
        zipCode = null
        district = null
        receiverName = null
        phone = null
    }

}

class ActiveOrdersResponseHydraMemberInvoiceAddress {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("address")
    var address : String? = null

    @SerializedName("country")
    var country : String? = null

    @SerializedName("county")
    var county : String? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("zipCode")
    var zipCode : String? = null

    @SerializedName("district")
    var district : String? = null

    @SerializedName("receiverName")
    var receiverName : String? = null

    @SerializedName("phone")
    var phone : String? = null

    init {
        name = null
        address = null
        country = null
        county = null
        city = null
        zipCode = null
        district = null
        receiverName = null
        phone = null
    }

}