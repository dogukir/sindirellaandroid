package com.company.sindirella.helpers

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.response.Order.ActiveOrdersResponseHydraMember
import com.company.sindirella.response.Order.ActiveOrdersResponseHydraMemberOrderDetail
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class OrderSummaryViewPagerAdapter(private var context: Context,private var data: ArrayList<ActiveOrdersResponseHydraMemberOrderDetail>) : PagerAdapter() {

    private var imageView: ImageView? = null
    private var brandNameText: TextView? = null
    private var productName: TextView? = null

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val view = LayoutInflater.from(context).inflate(R.layout.order_summary_viewpager_view,container,false)

        imageView = view.findViewById(R.id.imageView)
        brandNameText = view.findViewById(R.id.brandNameText)
        productName = view.findViewById(R.id.productName)

        if (!data.get(position).product?.pictures.isNullOrEmpty()){

            val requestOptions = RequestOptions()
            requestOptions.transform(CenterCrop(), GranularRoundedCorners(20f,20f,20f,20f))

            Glide.with(context).load(data.get(position).product?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(imageView!!)

        }

        brandNameText?.text = data.get(position).product?.brand?.name
        productName?.text = data.get(position).product?.name


        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return data.size
    }


}