package com.company.sindirella.helpers

import android.content.Context
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.response.OrderDetail.GetOrderDetailResponseHydraMember
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class BasketRecycleAdapter(private val context: Context?, private var data : ArrayList<GetOrderDetailResponseHydraMember>): RecyclerView.Adapter<BasketRecycleAdapter.ViewHolder>() {


    private var imageClothes : ImageView? = null
    private var textBrand : TextView? = null
    private var textTitle : TextView? = null
    private var textCargo : TextView? = null
    private var textPrices : TextView? = null
    private var trashImage : ImageView? = null
    private var mainLayout : ConstraintLayout? = null

    var deleteProductListener: DeleteProductListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.basket_items_view,parent,false)

        imageClothes = v.findViewById(R.id.imageClothes)
        textBrand = v.findViewById(R.id.textBrand)
        textTitle = v.findViewById(R.id.textTitle)
        textCargo = v.findViewById(R.id.textCargo)
        textPrices = v.findViewById(R.id.textPrices)
        trashImage = v.findViewById(R.id.trashImage)
        mainLayout = v.findViewById(R.id.mainLayout)




        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context!!,R.color.white)).cornerRadius(40).build()

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        textBrand?.text = data.get(position).product?.brand?.name
        textTitle?.text = data.get(position).product?.name

        if (!data.get(position).product?.pictures.isNullOrEmpty()){

            var requestOptions = RequestOptions()
            requestOptions.transform(CenterCrop(), GranularRoundedCorners(40f,0f,0f,40f))

            Glide.with(context!!).load(data.get(position).product?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(imageClothes!!)

        }

        textCargo?.text = "Kargo Bedava"

        textPrices?.text = data.get(position).amountTotal?.toFloat().toString() + " ₺"

        trashImage?.setOnClickListener {

            deleteProductListener?.deleteProduct(data.get(position).id)

        }

        var drawable = context?.resources?.getDrawable(R.drawable.trash_otuziki)
        drawable?.setColorFilter(context?.resources?.getColor(R.color.red)!!,PorterDuff.Mode.SRC_ATOP)

        trashImage?.background = drawable


        holder.setIsRecyclable(false)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

    interface DeleteProductListener {
        fun deleteProduct(id: Int?)
    }

}