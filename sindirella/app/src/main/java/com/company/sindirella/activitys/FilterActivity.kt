package com.company.sindirella.activitys

import android.app.ActionBar
import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.company.sindirella.R
import com.company.sindirella.controller.*
import com.company.sindirella.helpers.*
import com.company.sindirella.response.Activity.ActivitysResponse
import com.company.sindirella.response.Brand.BrandsResponse
import com.company.sindirella.response.Brand.BrandsResponseHydraMember
import com.company.sindirella.response.Category.CategoriesResponse
import com.company.sindirella.response.Category.CategoriesResponseHydraMember
import com.company.sindirella.response.Color.ColorsResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Size.SizeResponse
import com.company.sindirella.response.SizeGroup.SizeGroupResponse
import com.company.sindirella.response.SizeGroup.SizeGroupResponseHydraMember
import com.google.gson.Gson
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.spinner_item.view.*
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.sign

class FilterActivity : AppCompatActivity(),CategoryController.CategoriesListener, SizeController.SizesListener, ColorController.ColorsListener, BrandController.BrandsListener, ActivityController.ActivitiesListener, SizeGroupController.SizeGroupListener, FilterSizeGroupsBaseAdapter.SendSelectedSizeListener, FilterColorBaseAdapter.SendSelectedColorListener, FilterEventsGridViewAdapter.SendSelectedActivityListener {


    private var closeImage: ImageView? = null
    private var pageTitle: TextView? = null
    private var categoriesTitle: TextView? = null
    private var categoriesSpinner: Spinner? = null
    private var sizeTitle: TextView? = null
    private var sizeGrid: GridView? = null
    private var colorTitle: TextView? = null
    private var colorGrid: GridView? = null
    private var brandTitle: TextView? = null
    private var brandSpinner: Spinner? = null
    private var whichDaysTitle: TextView? = null
    private var startDateText: TextView? = null
    private var endDateText: TextView? = null
    private var priceRangeTitle: TextView? = null
    private var startPriceEdit: EditText? = null
    private var endPriceEdit: EditText? = null
    private var activitySubjectTitle: TextView? = null
    private var activitySubjectGrid: GridView? = null
    private var applyButton: Button? = null
    private var brandSpinnerLayout: RelativeLayout? = null
    private var categoriesSpinnerLayout: RelativeLayout? = null


    private var categoriesSpinnerArray : ArrayList<String>? = null
    private var brandsSpinnerArray: ArrayList<String>? = null


    private var categoryController: CategoryController? = null
    private var sizeController: SizeController? = null
    private var colorController: ColorController? = null
    private var brandController: BrandController? = null
    private var activityController: ActivityController? = null
    private var sizeGroupController: SizeGroupController? = null


    private var filterSizeGroupsBaseAdapter: FilterSizeGroupsBaseAdapter? = null
    private var filterColorBaseAdapter: FilterColorBaseAdapter? = null
    private var filterEventsGridViewAdapter : FilterEventsGridViewAdapter? = null


    var categoriesData: ArrayList<CategoriesResponseHydraMember>? = null
    var brandsData: ArrayList<BrandsResponseHydraMember>? = null
    var sizesData: ArrayList<SizeGroupResponseHydraMember>? = null


    var selectedCategoryId: Int? = null
    var selectedSizeId: ArrayList<String>? = null
    var selectedColorId: ArrayList<String>? = null
    var selectedBrandId: Int? = null
    var selectedStartDate: String? = null
    var selectedEndDate: String? = null
    var selectedMinAmount: String? = null
    var selectedMaxAmount: String? = null
    var selectedEventsId: ArrayList<String>? = null

    var activitiesData: ActivitysResponse? = null
    var colorData: ColorsResponse? = null
    var sizeGroups: SizeGroupResponse? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        supportActionBar?.hide()

        GlobalData.filterClick = false

        // filter params
        GlobalData.selectedMinAmount = null
        GlobalData.selectedMaxAmount = null
        GlobalData.selectedCategoryId = null
        GlobalData.selectedSizeId = null
        GlobalData.selectedColorId = null
        GlobalData.selectedBrandId = null
        GlobalData.selectedStartDate = null
        GlobalData.selectedEndDate = null
        GlobalData.selectedEventsId = null

        closeImage = findViewById(R.id.closeImage)
        pageTitle = findViewById(R.id.pageTitle)
        categoriesTitle = findViewById(R.id.categoriesTitle)
        categoriesSpinner = findViewById(R.id.categoriesSpinner)
        sizeTitle = findViewById(R.id.sizeTitle)
        sizeGrid = findViewById(R.id.sizeGrid)
        colorTitle = findViewById(R.id.colorTitle)
        colorGrid = findViewById(R.id.colorGrid)
        brandTitle = findViewById(R.id.brandTitle)
        brandSpinner = findViewById(R.id.brandSpinner)
        whichDaysTitle = findViewById(R.id.whichDaysTitle)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        priceRangeTitle = findViewById(R.id.priceRangeTitle)
        startPriceEdit = findViewById(R.id.startPriceEdit)
        endPriceEdit = findViewById(R.id.endPriceEdit)
        activitySubjectTitle = findViewById(R.id.activitySubjectTitle)
        activitySubjectGrid = findViewById(R.id.activitySubjectGrid)
        applyButton = findViewById(R.id.applyButton)
        brandSpinnerLayout = findViewById(R.id.brandSpinnerLayout)
        categoriesSpinnerLayout = findViewById(R.id.categoriesSpinnerLayout)



        categoryController = CategoryController(this,null)
        categoryController?.categoriesListener = this
        categoryController?.categories(null,null)

        sizeController = SizeController(this,null)
        sizeController?.sizesListener = this
        sizeController?.sizes(null,null,null)

        colorController = ColorController(this,null)
        colorController?.colorsListener = this
        colorController?.colors(null)

        brandController = BrandController(this,null)
        brandController?.brandsListener = this
        brandController?.brands(null)

        activityController = ActivityController(this,null)
        activityController?.activitiesListener = this
        activityController?.activities(null)

        sizeGroupController = SizeGroupController(this,null)
        sizeGroupController?.sizeGroupListener = this
        sizeGroupController?.sizeGroup(null)


        closeImage?.setOnClickListener {

            GlobalData.selectedMinAmount = null
            GlobalData.selectedMaxAmount = null
            GlobalData.selectedCategoryId = null
            GlobalData.selectedSizeId = null
            GlobalData.selectedColorId = null
            GlobalData.selectedBrandId = null
            GlobalData.selectedStartDate = null
            GlobalData.selectedEndDate = null
            GlobalData.selectedEventsId = null
            GlobalData.filterClick = null

            onBackPressed()

        }


        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startDateText?.text = sdf.format(calendarStart.time)
                startDateText?.setTextColor(resources.getColor(R.color.black))

                selectedStartDate = startDateText?.text.toString()

            } else {

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)

                selectedEndDate = endDateText?.text.toString()

            }
        }

        startDateText?.setOnClickListener {
            controllVal = 1
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(
                Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        endDateText?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(
                Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }


        /*backPressedText?.setOnClickListener {

            onBackPressed()

        }

         */


        categoriesSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position != 0){

                    selectedCategoryId = categoriesData?.get(position-1)?.id

                }else{
                    parent?.getChildAt(0)?.spinnerTextView?.setTextColor(resources.getColor(R.color.grey))

                    selectedCategoryId = null

                }

            }

        }

        brandSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position != 0){

                    selectedBrandId = brandsData?.get(position-1)?.id

                }else{

                    parent?.getChildAt(0)?.spinnerTextView?.setTextColor(resources.getColor(R.color.grey))
                    selectedBrandId = null

                }

            }

        }

        applyButton?.setOnClickListener {

            GlobalData.selectedMinAmount = startPriceEdit?.text.toString()
            GlobalData.selectedMaxAmount = endPriceEdit?.text.toString()
            GlobalData.selectedCategoryId = selectedCategoryId
            GlobalData.selectedSizeId = selectedSizeId
            GlobalData.selectedColorId = selectedColorId
            GlobalData.selectedBrandId = selectedBrandId
            GlobalData.selectedStartDate = selectedStartDate
            GlobalData.selectedEndDate = selectedEndDate
            GlobalData.selectedEventsId = selectedEventsId

            GlobalData.filterClick = true

            onBackPressed()

        }

        setUI()


    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (GlobalData.filterClick == true){

        }else{
            GlobalData.selectedMinAmount = null
            GlobalData.selectedMaxAmount = null
            GlobalData.selectedCategoryId = null
            GlobalData.selectedSizeId = null
            GlobalData.selectedColorId = null
            GlobalData.selectedBrandId = null
            GlobalData.selectedStartDate = null
            GlobalData.selectedEndDate = null
            GlobalData.selectedEventsId = null
            GlobalData.filterClick = null
        }

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        applyButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(this,R.color.sindirella_blue)).cornerRadius(20).build()
        startDateText?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        endDateText?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        startPriceEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        endPriceEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        brandSpinnerLayout?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        categoriesSpinnerLayout?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()

    }

    override fun getCategories(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                categoriesData = ArrayList()

                categoriesSpinnerArray = ArrayList()

                categoriesSpinnerArray?.add("Kategoriler")

                var categoriesData = Gson().fromJson(jsonElement,CategoriesResponse::class.java)

                this.categoriesData = categoriesData.hydraMember

                if (!categoriesData.hydraMember.isNullOrEmpty()){

                    for (i in 0..categoriesData.hydraMember?.size!!-1){

                        categoriesSpinnerArray?.add(categoriesData.hydraMember?.get(i)?.name.toString())

                    }


                    var spinnerAdapter = ArrayAdapter(this,R.layout.spinner_item, R.id.spinnerTextView,categoriesSpinnerArray!!)

                    categoriesSpinner?.adapter = spinnerAdapter
                    //categoriesSpinner?.setSelection(0)


                }




            }else{


            }


        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getSizes(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                var sizesData = Gson().fromJson(jsonElement,SizeResponse::class.java)




            }



        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getColors(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                colorData = Gson().fromJson(jsonElement,ColorsResponse::class.java)

                if (!colorData?.hydraMember.isNullOrEmpty()){

                    filterColorBaseAdapter = FilterColorBaseAdapter(this,colorData?.hydraMember!!)
                    filterColorBaseAdapter?.sendSelectedColorListener = this

                    colorGrid?.adapter = filterColorBaseAdapter

                    colorGrid?.numColumns = 6
                    colorGrid?.horizontalSpacing = 10
                    colorGrid?.verticalSpacing = 20

                }

            }else{



            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun selectedColorListener(selectedArray: java.util.ArrayList<Int>) {

        selectedColorId = ArrayList()

        for (i in 0 until selectedArray.size){

            selectedColorId?.add(colorData?.hydraMember?.get(selectedArray.get(i))?.id.toString())

        }

        //selectedColorId = selectedArray

    }

    override fun getBrands(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                brandsData = ArrayList()

                brandsSpinnerArray = ArrayList()

                brandsSpinnerArray?.add("Markalar")

                var brandsData = Gson().fromJson(jsonElement,BrandsResponse::class.java)

                this.brandsData = brandsData.hydraMember

                if (!brandsData.hydraMember.isNullOrEmpty()){

                    for (i in 0..brandsData.hydraMember?.size!!-1){

                        brandsSpinnerArray?.add(brandsData.hydraMember?.get(i)?.name.toString())

                    }

                    var spinnerAdapter = ArrayAdapter(this,R.layout.spinner_item, R.id.spinnerTextView,brandsSpinnerArray!!)

                    brandSpinner?.adapter = spinnerAdapter
                    //brandSpinner?.setSelection(0)


                }


            }else{


            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getActivities(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            //val collectionType = object : TypeToken<ArrayList<ActivitysResponse>>(){}.type
            //gson.fromJson(it,collectionType)

            //var activitiesData = ArrayList<ActivitysResponse>()

            //activitiesData = Gson().fromJson(jsonArray,collectionType)

            activitiesData = Gson().fromJson(jsonElement, ActivitysResponse::class.java)

            if (!activitiesData?.hydraMember.isNullOrEmpty()){

                filterEventsGridViewAdapter = FilterEventsGridViewAdapter(this,activitiesData?.hydraMember!!)
                filterEventsGridViewAdapter?.sendSelectedActivityListener = this

                activitySubjectGrid?.adapter = filterEventsGridViewAdapter

                activitySubjectGrid?.numColumns = 2
                activitySubjectGrid?.horizontalSpacing = 10
                activitySubjectGrid?.verticalSpacing = 10

            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun selectedActivityListener(selectedArray: java.util.ArrayList<Int>) {

        selectedEventsId = ArrayList()

        for (i in 0 until selectedArray.size){

            selectedEventsId?.add(activitiesData?.hydraMember?.get(selectedArray.get(i))?.id.toString())

        }

        //selectedEventsId = selectedArray

    }

    override fun getSizeGroup(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                sizeGroups = Gson().fromJson(jsonElement,SizeGroupResponse::class.java)

                if (!sizeGroups?.hydraMember.isNullOrEmpty()){

                    sizesData = ArrayList()
                    sizesData = sizeGroups?.hydraMember

                    filterSizeGroupsBaseAdapter = FilterSizeGroupsBaseAdapter(this,sizeGroups?.hydraMember!!)
                    filterSizeGroupsBaseAdapter?.sendSelectedSizeListener = this

                    sizeGrid?.adapter = filterSizeGroupsBaseAdapter

                    sizeGrid?.numColumns = 6
                    sizeGrid?.horizontalSpacing = 10
                    sizeGrid?.verticalSpacing = 20

                }

            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }


    override fun selectedSizeListener(selectedArray: java.util.ArrayList<Int>) {

        selectedSizeId = ArrayList()

        for (i in 0 until selectedArray.size){

            selectedSizeId?.add(sizeGroups?.hydraMember?.get(selectedArray.get(i))?.id.toString())

        }

        //selectedSizeId = selectedArray

    }
}