package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.response.Product.ProductResponseHydraMember
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class BrowserFragmentViewPagerAdapter(private var context: Context, private var data: ArrayList<ProductResponseHydraMember>, var favData: GetFavProductResponse?) : PagerAdapter() {

    private var mainLayout: ConstraintLayout? = null
    private var clothesView: RelativeLayout? = null
    private var imageViewClothes: ImageView? = null
    private var imageLike: ImageView? = null
    private var brandName: TextView? = null
    private var clothesName: TextView? = null
    private var priceLayout: LinearLayout? = null
    private var pricesNew: TextView? = null
    private var viewLinePrices: View? = null
    private var pricesOld: TextView? = null
    private var examinedButton: Button? = null

    private var viewButton: View? = null

    var clickViewPagerItemListener: ClickViewPagerItemListener? = null
    var browseViewPagerAdapterClickListener: BrowseViewPagerAdapterClickListener? = null

    var productData : ArrayList<ProductResponseHydraMember>? = ArrayList()


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.browser_viewpager_view_two,container,false)

        clothesView = layout.findViewById(R.id.clothesView)
        imageViewClothes = layout.findViewById(R.id.imageViewClothes)
        imageLike = layout.findViewById(R.id.imageLike)
        brandName = layout.findViewById(R.id.brandName)
        clothesName = layout.findViewById(R.id.clothesName)
        priceLayout = layout.findViewById(R.id.priceLayout)
        pricesNew = layout.findViewById(R.id.pricesNew)
        viewLinePrices = layout.findViewById(R.id.viewLinePrices)
        pricesOld = layout.findViewById(R.id.pricesOld)
        mainLayout = layout.findViewById(R.id.mainLayout)
        examinedButton = layout.findViewById(R.id.examinedButton)
        viewButton = layout.findViewById(R.id.viewButton)

        var likeDislikeBool = false


        var requestOptions = RequestOptions()
        requestOptions.transform(CenterCrop(), GranularRoundedCorners(20f,20f,0f,0f))

        Glide.with(context).load(productData?.get(position)?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(imageViewClothes!!)
        //imageViewClothes?.background = DrawableBuilder().cornerRadii(50,50,0,0).build()
        brandName?.text = productData?.get(position)?.brand?.name

        var lengthName = productData?.get(position)?.name?.length

        /*if (lengthName!! > 20){

            clothesName?.textSize = 13f

        }else if (lengthName > 30){

            clothesName?.textSize = 12f

        }

         */

        if (favData != null){

            var favControl = false

            for (i in 0..favData?.hydraMember?.size!!-1){

                if (productData?.get(position)?.id == favData?.hydraMember?.get(i)?.product?.id){

                    favControl = true

                }

            }

            if (favControl == true){

                likeDislikeBool = true
                imageLike?.background = ContextCompat.getDrawable(context,R.drawable.heart_like)

            }else{

                imageLike?.background = ContextCompat.getDrawable(context,R.drawable.heart_dislike)

            }

        }

        clothesName?.text = productData?.get(position)?.name
        pricesNew?.text = "${productData?.get(position)?.priceRental.toString()} ₺"
        pricesOld?.text = productData?.get(position)?.priceSale.toString() + " ₺"
        pricesOld?.paintFlags = (Paint.STRIKE_THRU_TEXT_FLAG)

        viewButton?.setOnClickListener {

            clickViewPagerItemListener?.clickViewPagerItem(productData?.get(position)?.BigId.toString())
            //popularClick?.popularClick(productResponse.hydraMember?.get(position)?.BigId)

        }

        imageLike?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(context, LoginActivity::class.java)
                context.startActivity(intent)
            }else{

                if (likeDislikeBool == true){

                    for (i in 0..favData?.hydraMember?.size!!-1){

                        if (productData?.get(position)?.id == favData?.hydraMember?.get(i)?.product?.id){

                            browseViewPagerAdapterClickListener?.disLikeClickBrowseViewPager(favData?.hydraMember?.get(i)?.id,position)

                        }

                    }

                }else{

                    browseViewPagerAdapterClickListener?.likeClickBrowseViewPager(productData?.get(position)?.id,position)

                    likeDislikeBool = true

                }

            }


        }

        setUI()


        container.addView(layout)

        return layout
    }

    fun setUI(){

        //imageViewClothes?.background = DrawableBuilder().cornerRadius(50).build()
        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(20).build()
        examinedButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black)).cornerRadius(20).build()
        viewButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black)).cornerRadius(20).build()

    }

    fun changeFav(favData: GetFavProductResponse, likePosition: Int?){

        this.favData = favData

        //notifyDataSetChanged()
        //notifyDataSetChanged()

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun getCount(): Int {

        return productData?.size!!
    }

    fun add(productVal: ProductResponseHydraMember) {

        /*if (preferencesQuery.id == -1){

        }else{
            items.add(preferencesQuery)
            filteredList.add(preferencesQuery)
            notifyDataSetChanged()
        }*/

        productData?.add(productVal)
        notifyDataSetChanged()

    }

    fun clearItems(){

        notifyDataSetChanged()

    }

    fun addAll(queries: ArrayList<ProductResponseHydraMember>) {

        if (!queries.isNullOrEmpty()) {

            for (query in queries) {

                add(query)

            }

        }

    }





    interface ClickViewPagerItemListener {
        fun clickViewPagerItem(BigId: String)
    }

    interface BrowseViewPagerAdapterClickListener {
        fun likeClickBrowseViewPager(id: Int?,position: Int?)
        fun disLikeClickBrowseViewPager(id: Int?,position: Int?)
    }


}