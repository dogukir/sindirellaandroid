package com.company.sindirella.response.Media

import com.google.gson.annotations.SerializedName

class PostMediaResponse {


    @SerializedName("@context")
    var BigIdContext: String? = null

    @SerializedName("@id")
    var BigIdId: String? = null

    @SerializedName("@type")
    var BigIdType: String? = null

    @SerializedName("id")
    var id: Int? = null

    init {
        id = null
        BigIdContext = null
        BigIdId = null
        BigIdType = null

    }

}