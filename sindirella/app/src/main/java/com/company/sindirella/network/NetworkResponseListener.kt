package com.company.sindirella.network

import com.company.sindirella.response.ErrorResponse

interface NetworkResponseListener<Response> {
    fun onResponseReceived(response: Response)
    fun onEmptyResponse(response: Response?)
    fun onError(failMessage: Int,error: ErrorResponse?)
}