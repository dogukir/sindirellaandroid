package com.company.sindirella.activitys

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.view.MenuItemView
import java.util.ArrayList

class MyAccountActivity : AppCompatActivity(),MenuItemView.SendScreenNameListener {

    private var backPressedText: ImageView? = null
    private var addView: LinearLayout? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        supportActionBar?.hide()

        addView = findViewById(R.id.addView)
        backPressedText = findViewById(R.id.backPressedText)

        addView?.removeAllViews()


        var menuArray = ArrayList<String>()

        menuArray.add("Siparişlerim")
        menuArray.add("Sipariş Geçmişim")
        menuArray.add("Adreslerim")
        menuArray.add("Ayarlar")

        for (i in 0..menuArray.size-1){

            val menuView = MenuItemView(this)
            menuView.sendScreenNameListener = this
            menuView.setTitle(menuArray.get(i))

            addView?.addView(menuView)

        }



        backPressedText?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }

    override fun sendScreenName(screenName: String?) {



        when(screenName){

            "Siparişlerim" -> {

                val intent = Intent(applicationContext,MyOrdersActivity::class.java)
                startActivity(intent)

            }

            "Sipariş Geçmişim" -> {

                val intent = Intent(applicationContext,MyOrderHistoryActivity::class.java)
                startActivity(intent)

            }

            "Adreslerim" -> {

                val intent = Intent(applicationContext,AddressesActivity::class.java)
                startActivity(intent)

            }

            "Ayarlar" -> {

                val intent = Intent(applicationContext,SettingsActivity::class.java)
                startActivity(intent)

            }

        }

    }
}