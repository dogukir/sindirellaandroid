package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestProduct
import com.company.sindirella.requestApi.RequestProductId
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class ProductController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var productListener: ProductListener? = null

    fun product(id: Int?, idArray: ArrayList<Int>?, brand: String?,
                brandArray: ArrayList<String>?, color: String?, colorArray: ArrayList<String>?,
                activities: String?, activitiesArray: ArrayList<String>?,
                inventoriesSize: String?, inventoriesSizeArray: ArrayList<String>?,
                categories: String?, categoriesArray: ArrayList<String>?,
                description: String?, name: String?, priceRentalBetween: String?,
                priceRentalGt: String?, priceRentalGte: String?,
                priceRentalLt: String?, priceRentalLte: String?,
                exist: Boolean?, orderPriceRental: String?, orderPopularity: String?,
                page: Int?,orderId: String?,startDate:String?,endDate: String?){

        RequestProduct(activity,fragment,id,idArray,brand,brandArray,color,colorArray,activities,activitiesArray,inventoriesSize,inventoriesSizeArray,categories,categoriesArray,description,name,priceRentalBetween,priceRentalGt,
            priceRentalGte,priceRentalLt,priceRentalLte,exist,orderPriceRental,orderPopularity,page,orderId,startDate,endDate,object : NetworkResponseListener<JsonElement>{
                override fun onResponseReceived(response: JsonElement) {
                    productListener?.getProduct(true,response,-1,null)
                }

                override fun onEmptyResponse(response: JsonElement?) {
                    productListener?.getProduct(true,null,-1,null)
                }

                override fun onError(failMessage: Int, error: ErrorResponse?) {
                    productListener?.getProduct(false,null,failMessage,error)
                }

            })

    }



    fun productId(id: String?){
        RequestProductId(activity,fragment,id,object: NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                productListener?.getProductId(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                productListener?.getProductId(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                productListener?.getProductId(false,null,failMessage,error)
            }

        })
    }



    interface ProductListener {
        fun getProduct(response: Boolean?,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun getProductId(responseOk: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }



}