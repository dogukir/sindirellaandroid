package com.company.sindirella.activitys

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.CommentController
import com.company.sindirella.helpers.CommentActivityRecyclerAdapter
import com.company.sindirella.response.Comment.GetCommentsResponse
import com.company.sindirella.response.ErrorResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import top.defaults.drawabletoolbox.DrawableBuilder
import java.io.Serializable
import java.util.ArrayList

class CommentsActivity : AppCompatActivity(),CommentController.CommentsListener, CommentActivityRecyclerAdapter.CommentClickListener {

    private var recyclerView : RecyclerView? = null
    private var buttonComment : Button? = null
    private var adapter : CommentActivityRecyclerAdapter? = null
    private var backPressedText : TextView? = null
    private var brandNameText: TextView? = null
    private var productNameText: TextView? = null
    private var commentSizeText: TextView? = null

    private var commentController: CommentController? = null

    var productId: String? = null
    var brandName: String? = null
    var productName: String? = null
    var productIdUrl: String? = null

    var commentsData = GetCommentsResponse()


    override fun onResume() {

        if (productId != null && !productId.equals("")){

            commentController?.comments(null,null,null,null,null,productId,null,"DESC",1)

        }

        super.onResume()
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments)
        supportActionBar?.hide()

        productId = intent.getStringExtra("productId")
        brandName = intent.getStringExtra("brandName")
        productName = intent.getStringExtra("productName")
        productIdUrl = intent.getStringExtra("productIdUrl")

        recyclerView = findViewById(R.id.recyclerView)
        buttonComment = findViewById(R.id.buttonComment)
        backPressedText = findViewById(R.id.backPressedText)
        brandNameText = findViewById(R.id.brandNameText)
        productNameText = findViewById(R.id.productNameText)
        commentSizeText = findViewById(R.id.commentSizeText)

        recyclerView?.isNestedScrollingEnabled = true


        commentController = CommentController(this,null)
        commentController?.commentsListener = this

        if (productId != null && !productId.equals("")){

            commentController?.comments(null,null,null,null,null,productId,null,"DESC",1)

        }





        var arr = ArrayList<String>()

        arr.add("doğukan")
        arr.add("batıkan")
        arr.add("kuzeykan")
        arr.add("ağlakan")

        //adapter = CommentActivityRecyclerAdapter(this,arr)
        //adapter?.commentClickListener = this
        //recyclerView?.adapter = adapter

        val Layout = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        recyclerView?.layoutManager = Layout





        backPressedText?.setOnClickListener {

            onBackPressed()

        }

        buttonComment?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
            }else{
                val intent = Intent(this,ToCommentActivity::class.java)
                intent.putExtra("productIdUrl",productIdUrl)
                startActivity(intent)
            }

        }


        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        buttonComment?.background = DrawableBuilder().solidColor(resources.getColor(R.color.sindirella_blue)).cornerRadius(20).build()

    }

    override fun commentClick(id: Int?) {

        if (appPreferences.token.equals("")){
            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }else{
            val intent = Intent(this,ToCommentActivity::class.java)
            startActivity(intent)
        }

    }

    override fun commentDetailClick(position: Int?) {

        val intent = Intent(this,CommentDetail::class.java)
        intent.putExtra("obj",commentsData.hydraMember?.get(position!!))
        startActivity(intent)

    }

    override fun getComments(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            commentsData = Gson().fromJson(jsonElement, GetCommentsResponse::class.java)

            brandNameText?.text = brandName
            productNameText?.text = productName

            if (!commentsData.hydraMember.isNullOrEmpty()){

                commentSizeText?.text = commentsData.hydraMember?.size.toString() + " yorum"


                adapter = CommentActivityRecyclerAdapter(this,commentsData.hydraMember!!)
                adapter?.commentClickListener = this
                recyclerView?.adapter = adapter



            }else if (commentsData.hydraMember?.size == 0){

                commentSizeText?.text = "0 yorum"

            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription, Toast.LENGTH_LONG).show()

        }

    }

}