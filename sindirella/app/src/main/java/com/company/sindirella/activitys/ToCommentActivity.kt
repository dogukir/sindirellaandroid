package com.company.sindirella.activitys

import android.Manifest
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.text.htmlEncode
import androidx.loader.content.CursorLoader
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.CommentController
import com.company.sindirella.controller.MediaController
import com.company.sindirella.helpers.BrowseFragmentRecycleAdapter
import com.company.sindirella.helpers.CommentPhotosRecyclerAdapter
import com.company.sindirella.helpers.GlobalData
import com.company.sindirella.response.Comment.PostCommentsResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Media.PostMediaResponse
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import top.defaults.drawabletoolbox.DrawableBuilder
import java.io.File
import java.net.URI

class ToCommentActivity: AppCompatActivity(),CommentController.ToCommentListener , MediaController.MediaListener{

    override fun getMedia(
        response: Boolean?,
        jsonArray: JsonArray?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {



    }


    var asd = JsonObject()
    private var closeImage: ImageView? = null
    private var ratingStars: RatingBar? = null
    private var commentTitleEdit: EditText? = null
    private var commentEdit: EditText? = null
    private var addPhotoButton: Button? = null
    private var toCommentButton: Button? = null
    private var recyclerView: RecyclerView? = null
    private var PICK_IMAGE = 100
    private var imageUri : Uri? = null
    private var imageArrayUri: ArrayList<Uri>? = null
    private var imageJsonArr: JsonArray? = null


    private var gridLayoutManager: GridLayoutManager? = null
    private var adapter: CommentPhotosRecyclerAdapter? = null


    private var commentController: CommentController? = null
    private var mediaController: com.company.sindirella.controller.MediaController? = null

    private var productIdUrl: String? = null
    private var params: ViewGroup.LayoutParams? = null
    private var medias: JsonArray? = null

    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        commentTitleEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(2).cornerRadius(20).build()
        commentEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(2).cornerRadius(20).build()
        addPhotoButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.black)).strokeWidth(2).cornerRadius(20).build()
        toCommentButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.sindirella_blue)).cornerRadius(20).build()


    }


    fun isStoragePermissionGranted():Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(applicationContext!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
                Log.v("i","Permission is granted")

                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 15)
                Log.v("Test","Permission is revoked")

                return false
            }
        }
        else {
            Log.v("tesk","Permission is granted");

            return true;
        }
    }

    fun isReadStoragePermissionGranted():Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(applicationContext!!, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
                Log.v("i","Permission is granted")

                return true
            } else {
                ActivityCompat.requestPermissions(this@ToCommentActivity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 950)
                Log.v("Test","Permission is revoked")

                return false
            }
        }
        else {
            Log.v("tesk","Permission is granted");

            return true;
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_to_comment)
        supportActionBar?.hide()

        isReadStoragePermissionGranted()
        isStoragePermissionGranted()

        imageArrayUri = ArrayList()
        imageJsonArr = JsonArray()
        medias = JsonArray()

        productIdUrl = intent.getStringExtra("productIdUrl")

        closeImage = findViewById(R.id.closeImage)
        ratingStars = findViewById(R.id.ratingStars)
        commentTitleEdit = findViewById(R.id.commentTitleEdit)
        commentEdit = findViewById(R.id.commentEdit)
        addPhotoButton = findViewById(R.id.addPhotoButton)
        toCommentButton = findViewById(R.id.toCommentButton)
        recyclerView = findViewById(R.id.recyclerView)




        commentController = CommentController(this,null)
        commentController?.toCommentListener = this

        mediaController= MediaController(this,null)
        mediaController?.mediaListener = this






        gridLayoutManager = GridLayoutManager(this,2,GridLayoutManager.VERTICAL,false)
        recyclerView?.layoutManager = gridLayoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()


        toCommentButton?.setOnClickListener {

            var toCommentBody = JsonObject()

            toCommentBody.addProperty("product",productIdUrl)

            toCommentBody.add("medias",medias)

            if (!commentEdit?.text.isNullOrEmpty()){
                toCommentBody.addProperty("hasBody",true)
            }

            if (!commentTitleEdit?.text.isNullOrEmpty()){
                toCommentBody.addProperty("hasTitle",true)
            }

            if (ratingStars?.rating!! > 0){
                toCommentBody.addProperty("hasRating",true)
            }

            toCommentBody.addProperty("isActive",true)
            toCommentBody.addProperty("title",commentTitleEdit?.text.toString())
            toCommentBody.addProperty("body",commentEdit?.text.toString())
            toCommentBody.addProperty("rating",ratingStars?.rating!!.toInt())
            //toCommentBody.add("medias",imageJsonArr)

            if (commentTitleEdit?.text.isNullOrEmpty() || commentEdit?.text.isNullOrEmpty()){

                Toast.makeText(this,"Alanları doldurunuz.",Toast.LENGTH_LONG).show()

            }else{

                /*
                var multiPartArray = ArrayList<MultipartBody.Part>()
                var i = 0
                imageJsonArr = JsonArray()

                for(item in imageArrayUri!!) {

                    var tempFile = File(item.path)
                    imageJsonArr?.add(tempFile.name + ".png")
                    var requestBody = RequestBody.create(MediaType.parse("image/png"),tempFile)
                    var bodyObj = MultipartBody.Part.createFormData("imageFile",tempFile.name + ".png",requestBody)
                    multiPartArray.add(bodyObj)
                }

                mediaController?.postMedia(appPreferences.token,multiPartArray)

                 */

                asd = toCommentBody
                commentController?.postComment(appPreferences.token,asd)


            }

        }



        closeImage?.setOnClickListener {

            onBackPressed()

        }

        addPhotoButton?.setOnClickListener {

            var gallery = Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, PICK_IMAGE)

        }

        setUI()

    }

    override fun postToComment(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var toCommentData = Gson().fromJson(jsonElement,PostCommentsResponse::class.java)

            onBackPressed()


        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE){

            imageUri = data?.data

            var file = File(getRealPathFromUri(imageUri!!))

            var asd = RequestBody.create(MediaType.parse("image/jpg"),file)

            //var map = HashMap<String,RequestBody>()
            //map = HashMap()
            //map.put("imageFile",asd)

            var photo = MultipartBody.Part.createFormData("image",file.name,asd)

            mediaController?.postMedia(appPreferences.token,photo)

            imageArrayUri?.add(imageUri!!)

            params = recyclerView?.layoutParams
            params?.height = 400


            /*
            imageUri = data?.getData()
            imageArrayUri?.add(imageUri!!)
            imageJsonArr?.add(imageUri?.path)



            params = recyclerView?.layoutParams
            params?.height = 400

             */

            adapter = CommentPhotosRecyclerAdapter(this,imageArrayUri!!)
            recyclerView?.adapter = adapter
        }

    }

    /*fun getRealPathFromURI(contentUri: Uri) : String {
        var qwe = arrayOfNulls<String>(5)
        qwe[0] = MediaStore.Images.Media.DATA
        var loader = CursorLoader(applicationContext,contentUri,qwe,null,null,null);
        var cursor = loader.loadInBackground()
        var column_index = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor?.moveToFirst()
        var result = cursor?.getString(column_index!!)
        cursor?.close()
        return result.toString()
    }

     */

    fun getDataColumn(context: Context, uri:Uri, selection:String, selectionArgs:Array<String>?):String {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf<String>(column)
        try
        {
            cursor = context.getContentResolver().query(uri, null, null, null, null)
            //cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst())
            {
                val index = cursor.getColumnIndexOrThrow(column)
                //cursor.close()
                return cursor.getString(index)
            }
        }
        finally
        {
            if (cursor != null){

            }
                //cursor.close()
        }
        return ""
    }


    fun getRealPathFromUri(uri:Uri):String {
        // DocumentProvider
        var stringArr : Array<String>? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(applicationContext, uri))
        {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri))
            {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split((":").toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true))
                {
                    return "${Environment.getExternalStorageDirectory()}/" + split[1]
                }
            }
            else if (isDownloadsDocument(uri))
            {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                return getDataColumn(applicationContext!!, contentUri, "", stringArr!!)
            }
            else if (isMediaDocument(uri))
            {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split((":").toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                val type = split[0]
                var contentUri:Uri? = null
                if ("image" == type)
                {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }
                else if ("video" == type)
                {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                }
                else if ("audio" == type)
                {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf<String>(split[1])
                return getDataColumn(applicationContext!!, contentUri!!, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        }
        else if ("content".equals(uri.getScheme(), ignoreCase = true))
        {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.lastPathSegment.toString()
            return getDataColumn(applicationContext!!, uri, "", stringArr)
        }
        else if ("file".equals(uri.getScheme(), ignoreCase = true))
        {
            return uri.getPath().toString()
        }// File
        // MediaStore (and general)
        return ""
    }

    fun isExternalStorageDocument(uri:Uri):Boolean {
        return "com.android.externalstorage.documents" == uri.getAuthority()
    }
    fun isDownloadsDocument(uri:Uri):Boolean {
        return "com.android.providers.downloads.documents" == uri.getAuthority()
    }
    fun isMediaDocument(uri:Uri):Boolean {
        return "com.android.providers.media.documents" == uri.getAuthority()
    }
    fun isGooglePhotosUri(uri:Uri):Boolean {
        return "com.google.android.apps.photos.content" == uri.getAuthority()
    }



    override fun postMedia(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            if (jsonElement != null){

                var qwe = Gson().fromJson(jsonElement, PostMediaResponse::class.java)
                if(qwe.id != null){

                    medias?.add(qwe.BigIdId)

                }

            }else{



            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

        //commentController?.postComment(appPreferences.token,asd)

    }

}