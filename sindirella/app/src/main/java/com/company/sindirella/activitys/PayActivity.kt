package com.company.sindirella.activitys

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.ClientAddressController
import com.company.sindirella.controller.OrderDetailController
import com.company.sindirella.controller.PaymentController
import com.company.sindirella.response.ClientAddress.GetClientAddressResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.OrderDetail.GetOrderDetailResponse
import com.company.sindirella.response.OrderDetail.GetOrderDetailResponseHydraMember
import com.company.sindirella.response.Payment.PostPaymentResponse
import com.company.sindirella.view.AddressesCardView
import com.company.sindirella.view.payProductCardView
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import top.defaults.drawabletoolbox.DrawableBuilder

class PayActivity : AppCompatActivity(),ClientAddressController.ClientAddressesListener,OrderDetailController.OrderDetailsListener, PaymentController.PaymentListener {


    private var closeImage: ImageView? = null
    private var addAddressButton: Button? = null
    private var addressLayout: LinearLayout? = null
    private var invoiceCheckbox: CheckBox? = null
    private var centerInvoiceAddressLayout: LinearLayout? = null
    private var invoiceAddressLayout: LinearLayout? = null
    private var cardOwnerEdit: EditText? = null
    private var cardNumberEdit: EditText? = null
    private var lastUseDateEdit: EditText? = null
    private var cvvEdit: EditText? = null
    private var eftCheckbox: CheckBox? = null
    private var salesContractCheckbox: CheckBox? = null
    private var productLayout: LinearLayout? = null
    private var totalDataText: TextView? = null
    private var generalTotalDataText: TextView? = null
    private var payButton: Button? = null
    private var newAdressClick = false
    private var invoiceButton: Button? = null
    private var invoiceButtonClick = false
    private var eftButton: Button? = null
    private var eftButtonClick = false
    private var salesContractButton: Button? = null
    private var salesContractButtonClick = false
    private var selectedInvoiceAddress: Int? = null
    private var selectedShipmentAddress: Int? = null
    private var paymentType = 0
    private var salesContactBool = false
    private var cargoDataText: TextView? = null



    private var clientAddressController: ClientAddressController? = null
    private var orderDetailController: OrderDetailController? = null
    private var paymentController: PaymentController? = null

    override fun onResume() {
        super.onResume()

        if (newAdressClick){

            clientAddressController?.getClientAddresses(appPreferences.token,null)
            newAdressClick = false
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay)
        supportActionBar?.hide()


        closeImage = findViewById(R.id.closeImage)
        addAddressButton = findViewById(R.id.addAddressButton)
        addressLayout = findViewById(R.id.addressLayout)
        invoiceCheckbox = findViewById(R.id.invoiceCheckbox)
        centerInvoiceAddressLayout = findViewById(R.id.centerInvoiceAddressLayout)
        invoiceAddressLayout = findViewById(R.id.invoiceAddressLayout)
        cardOwnerEdit = findViewById(R.id.cardOwnerEdit)
        cardNumberEdit = findViewById(R.id.cardNumberEdit)
        lastUseDateEdit = findViewById(R.id.lastUseDateEdit)
        cvvEdit = findViewById(R.id.cvvEdit)
        eftCheckbox = findViewById(R.id.eftCheckbox)
        salesContractCheckbox = findViewById(R.id.salesContractCheckbox)
        productLayout = findViewById(R.id.productLayout)
        totalDataText = findViewById(R.id.totalDataText)
        generalTotalDataText = findViewById(R.id.generalTotalDataText)
        payButton = findViewById(R.id.payButton)
        invoiceButton = findViewById(R.id.invoiceButton)
        eftButton = findViewById(R.id.eftButton)
        salesContractButton = findViewById(R.id.salesContractButton)
        cargoDataText = findViewById(R.id.cargoDataText)



        clientAddressController = ClientAddressController(this,null)
        clientAddressController?.clientAddressesListener = this
        clientAddressController?.getClientAddresses(appPreferences.token,null)


        orderDetailController = OrderDetailController(this,null)
        orderDetailController?.orderDetailsListener = this
        orderDetailController?.getOrderDetails(appPreferences.token,null,null,0,null,"DESC",null)


        paymentController = PaymentController(this,null)
        paymentController?.paymentListener = this

        /*invoiceCheckbox?.setOnClickListener {

            if (invoiceCheckbox?.isChecked == true){

                centerInvoiceAddressLayout?.visibility = View.GONE

            }else{

                centerInvoiceAddressLayout?.visibility = View.VISIBLE

            }



        }

         */

        invoiceButton?.setOnClickListener {

            if (!invoiceButtonClick){

                invoiceButtonClick = true

                invoiceButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.sindirella_blue)).strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()
                centerInvoiceAddressLayout?.visibility = View.GONE

            }else{

                invoiceButtonClick = false

                invoiceButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()
                centerInvoiceAddressLayout?.visibility = View.VISIBLE

            }

        }

        eftButton?.setOnClickListener {

            if (eftButtonClick){

                eftButtonClick = false
                paymentType = 0
                eftButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()

            }else{

                eftButtonClick = true
                paymentType = 1
                eftButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.sindirella_blue)).strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()

            }



        }


        salesContractButton?.setOnClickListener {

            if (salesContractButtonClick){

                salesContractButtonClick = false
                salesContactBool = false
                salesContractButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()

            }else{

                salesContractButtonClick = true
                salesContactBool = true
                salesContractButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.sindirella_blue)).strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()

            }

        }


        payButton?.setOnClickListener {

            var body = JsonObject()

            if (selectedShipmentAddress == null || paymentType == 0 || salesContactBool == false){

                var message = ""

                if (selectedShipmentAddress == null){

                    message = message + "Adres Seçiniz"

                }

                if (paymentType == 0){

                    message = message + " ,Ödeme yöntemi seçiniz"

                }

                if (salesContactBool == false){

                    message = message + " ,Kullanıcı Sözleşmesini Onaylayın"

                }

                Toast.makeText(applicationContext,"Lütfen ${message}",Toast.LENGTH_LONG).show()

            }else{

                body.addProperty("card","")
                body.addProperty("coupon",false)
                body.addProperty("cvv","")
                body.addProperty("date","")

                if (selectedInvoiceAddress == null){

                    body.addProperty("invoiceAddress",selectedShipmentAddress)
                    body.addProperty("shipmentAddress",selectedShipmentAddress)

                }else{

                    body.addProperty("invoiceAddress",selectedInvoiceAddress)
                    body.addProperty("shipmentAddress",selectedShipmentAddress)

                }

                body.addProperty("invoiceType","person")
                body.addProperty("name","")
                body.addProperty("paymentType",paymentType)
                body.addProperty("sozlesme",salesContactBool)

                paymentController?.postPayment(appPreferences.token,body)

            }


        }



        addAddressButton?.setOnClickListener {

            newAdressClick = true

            val intent = Intent(applicationContext,AddressesDetailActivity::class.java)
            startActivity(intent)

        }

        closeImage?.setOnClickListener {

            onBackPressed()

        }



        setUI()

    }


    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        payButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.sindirella_blue)).cornerRadius(20).build()
        addAddressButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()

        invoiceButtonClick = true
        invoiceButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.sindirella_blue)).strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()
        //invoiceButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()
        eftButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()
        salesContractButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(applicationContext,R.color.black)).strokeWidth(2).cornerRadius(20).build()

    }

    override fun getClientAddresses(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var addressData = Gson().fromJson(jsonElement,GetClientAddressResponse::class.java)

            if (!addressData.hydraMember.isNullOrEmpty()){

                addressLayout?.removeAllViews()
                invoiceAddressLayout?.removeAllViews()

                for (i in 0..addressData.hydraMember?.size!!-1){

                    val view = AddressesCardView(applicationContext)

                    view.setDataForPay(addressData.hydraMember?.get(i))

                    view.setOnClickListener {

                        view.clickView()
                        selectedShipmentAddress = addressData.hydraMember?.get(i)?.id

                    }

                    addressLayout?.addView(view)

                }

                for (i in 0..addressData.hydraMember?.size!!-1){

                    val view = AddressesCardView(applicationContext)

                    view.setDataForPay(addressData.hydraMember?.get(i))

                    view.setOnClickListener {

                        view.clickView()
                        selectedInvoiceAddress = addressData.hydraMember?.get(i)?.id

                    }

                    invoiceAddressLayout?.addView(view)

                }

            }


        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getOrderDetails(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var orderDetailData = Gson().fromJson(jsonElement,GetOrderDetailResponse::class.java)

            if (!orderDetailData.hydraMember.isNullOrEmpty()){

                productLayout?.removeAllViews()

                var totallyPrice = 0

                for (i in 0..orderDetailData.hydraMember?.size!!-1){

                    val view = payProductCardView(applicationContext)

                    view.setData(orderDetailData.hydraMember?.get(i))

                    productLayout?.addView(view)

                    totallyPrice = totallyPrice + orderDetailData.hydraMember?.get(i)?.amountTotal!!

                }

                totalDataText?.text = totallyPrice.toFloat().toString() + " ₺"
                generalTotalDataText?.text = totallyPrice.toFloat().toString() + " ₺"
                cargoDataText?.text = "0 ₺"


            }



        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getPayment(response: Boolean?, jsonArray: JsonArray?, failMessage: Int?, error: ErrorResponse?) {

    }

    override fun postPayment(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            if (jsonElement != null){

                var data = Gson().fromJson(jsonElement, PostPaymentResponse::class.java)

                var intent = Intent(applicationContext,TruePayActivity::class.java)
                intent.putExtra("transactionId",data.theOrder?.transactionId)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()

            }else{



            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }













    override fun postClientAddresses(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {

    }

    override fun deleteClientAddresses(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        TODO("Not yet implemented")
    }

    override fun postCheckOrder(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

    }

    override fun postOrderDetails(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        TODO("Not yet implemented")
    }

    override fun deleteOrderDetailsId(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        TODO("Not yet implemented")
    }

}