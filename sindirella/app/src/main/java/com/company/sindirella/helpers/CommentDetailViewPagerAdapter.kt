package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.company.sindirella.R
import com.company.sindirella.response.Comment.GetCommentsResponseHydraMembersMedias
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class CommentDetailViewPagerAdapter(private val context: Context,private val data : ArrayList<GetCommentsResponseHydraMembersMedias>) : PagerAdapter() {

    private var viewTop : View? = null
    private var viewBottom : View? = null
    private var mainLayout : ConstraintLayout? = null
    private var clothesImage: ImageView? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val v = LayoutInflater.from(context)
        val layout = v.inflate(R.layout.commet_detail_item_view,container,false)

        viewTop = layout.findViewById(R.id.viewTop)
        viewBottom = layout.findViewById(R.id.viewBottom)
        mainLayout = layout.findViewById(R.id.mainLayout)
        clothesImage = layout.findViewById(R.id.clothesImage)

        Glide.with(context).load(data.get(position).imageUrl).centerCrop().into(clothesImage!!)


        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).build()
        viewTop?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.comment_detail_view_top)).cornerRadii(50,50,0,0).build()
        viewBottom?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.comment_detail_view_bottom)).cornerRadii(0,0,50,50).build()

        container.addView(layout)

        return layout
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }



    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return data.size
    }


}