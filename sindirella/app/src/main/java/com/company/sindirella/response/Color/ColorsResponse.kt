package com.company.sindirella.response.Color

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ColorsResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<ColorsResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class ColorsResponseHydraMember{

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("hex")
    var hex : String? = null

    init {
        id = null
        name = null
        hex = null
    }

}