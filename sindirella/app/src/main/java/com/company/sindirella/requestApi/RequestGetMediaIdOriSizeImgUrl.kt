package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Media.GetMediaIdOriSizeImgUrlResponse
import com.company.sindirella.response.Media.GetMediaIdOriSizeResponse

class RequestGetMediaIdOriSizeImgUrl(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<GetMediaIdOriSizeImgUrlResponse>) {

    init {
        val request = RequestCreator.create<Service.GetMediaIdOriSizeImgUrl>(Service.GetMediaIdOriSizeImgUrl::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMediaIdOriSizeImgUrl(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}