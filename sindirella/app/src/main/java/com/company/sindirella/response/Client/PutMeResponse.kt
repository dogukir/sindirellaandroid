package com.company.sindirella.response.Client

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class PutMeResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("email")
    var email : String? = null

    @SerializedName("phone")
    var phone : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("birthday")
    var birthday : String? = null

    @SerializedName("extra")
    var extra : ArrayList<String>? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("size")
    var size : String? = null

    @SerializedName("country")
    var country : String? = null

    init {
        id = null
        email = null
        phone = null
        name = null
        birthday = null
        extra = null
        city = null
        size = null
        country = null
    }

}