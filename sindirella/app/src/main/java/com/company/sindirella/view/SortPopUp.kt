package com.company.sindirella.view

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class SortPopUp(context: Context,private val data : ArrayList<String>) : Dialog(context, R.style.Theme_AppCompat_Light_Dialog) {

    private var sortLayout : RelativeLayout? = null
    var exitButton : Button? = null
    private var mainLayout : RelativeLayout? = null

    private var sortSuggested: TextView? = null
    private var sortNew: TextView? = null
    private var sortPriceGrowing: TextView? = null
    private var sortPriceDecreasing: TextView? = null

    var sortClickListener: SortClickListener? = null

    fun createView(layout: Int){

        if (layout == R.layout.sort_pop_up){

            val view = LayoutInflater.from(context).inflate(layout,null)

            sortLayout = view.findViewById(R.id.sortLayout)
            exitButton = view.findViewById(R.id.exitButton)
            mainLayout = view.findViewById(R.id.mainLayout)

            sortSuggested = view.findViewById(R.id.sortSuggested)
            sortNew = view.findViewById(R.id.sortNew)
            sortPriceGrowing = view.findViewById(R.id.sortPriceGrowing)
            sortPriceDecreasing = view.findViewById(R.id.sortPriceDecreasing)

            sortSuggested?.setOnClickListener {

                sortClickListener?.sortClickPopUp("order[popularity]","ASC")

            }

            sortNew?.setOnClickListener {

                sortClickListener?.sortClickPopUp("order[id]","DESC")

            }

            sortPriceGrowing?.setOnClickListener {

                sortClickListener?.sortClickPopUp("order[priceRental]","ASC")

            }

            sortPriceDecreasing?.setOnClickListener {

                sortClickListener?.sortClickPopUp("order[priceRental]","DESC")

            }



            this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.transparent))
            this.window?.clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND)
            this.getWindow()?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)

            mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.transparent)).cornerRadius(50).build()
            sortLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.view_background_trasparent)).cornerRadius(30).build()
            exitButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(30).build()


            setContentView(view)

        }

    }

    interface SortClickListener {
        fun sortClickPopUp(key: String?,sort: String?)
    }

}