package com.company.sindirella.response.Media

import com.google.gson.annotations.SerializedName

class GetMediaIdOriSizeImgUrlResponse {

    @SerializedName("id")
    var id: Int? = null

    init {
        id = null
    }

}