package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.HelpPage.HelpPageIdResponse
import com.company.sindirella.response.Invertory.InventoriesResponse
import retrofit2.http.Query
import java.util.ArrayList

class RequestInventories(activity: AppCompatActivity?, fragment: Fragment?, id: Int?,idArray: ArrayList<Int>?, size: String?, sizeArray: ArrayList<String>?, page: Int?, listener: NetworkResponseListener<ArrayList<InventoriesResponse>>) {

    init {
        val request = RequestCreator.create<Service.Inventories>(Service.Inventories::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getInventories(id,idArray,size,sizeArray,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}