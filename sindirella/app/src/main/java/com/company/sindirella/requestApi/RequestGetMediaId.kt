package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Media.GetMediaIdResponse
import com.company.sindirella.response.Media.GetMediaResponse
import java.util.ArrayList

class RequestGetMediaId(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<GetMediaIdResponse>) {

    init {
        val request = RequestCreator.create<Service.GetMediaId>(Service.GetMediaId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMediaId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}