package com.company.sindirella.network

import com.company.sindirella.response.ErrorResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class RequestErrorHandler(errorBody: okhttp3.ResponseBody?) {

    private var obj: ErrorResponse? = null
    private var error : ErrorResponse? = null

    val errorString: ErrorResponse?
        get() {

            try {

                if (obj != null) {
                    //var errorData = Gson().fromJson(obj,ErrorResponse::class.java)
                    error = obj

                }

            } catch (e: JSONException) {

                e.printStackTrace()

            }

            return error

        }

    init {

        try {

            if (errorBody != null) {

                val body = errorBody.string()

                if (body.length != 0) {
                    obj = Gson().fromJson(body,ErrorResponse::class.java)

                } else {

                    obj = null

                }

            } else {

                obj = null

            }


        } catch (e: IOException) {

            e.printStackTrace()

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

}