package com.company.sindirella.activitys

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.CityController
import com.company.sindirella.controller.ClientAddressController
import com.company.sindirella.controller.TownController
import com.company.sindirella.response.City.CitiesIdResponse
import com.company.sindirella.response.City.CitiesResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Town.TownIdResponse
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.spinner_item.view.*
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class AddressesDetailActivity : AppCompatActivity(),ClientAddressController.ClientAddressesListener,CityController.CitiesListener,TownController.TownsListener {

    private var closeImage: ImageView? = null
    private var addressesNameEdit: EditText? = null
    private var nameSurnameEdit: EditText? = null
    private var citySpinner: Spinner? = null
    private var districtSpinner: Spinner? = null
    private var neighborhoodSpinner: Spinner? = null
    private var addressesEdit: EditText? = null
    private var postCodeEdit: EditText? = null
    private var phoneEdit: EditText? = null
    private var saveButton: Button? = null

    var addressesBody = JsonObject()

    var cityData: CitiesResponse? = null
    var countiesOfCityData: CitiesIdResponse? = null
    var districtsData: TownIdResponse? = null

    var cityNames: ArrayList<String>? = null
    var countiesOfCityNames: ArrayList<String>? = null
    var districtNames: ArrayList<String>? = null

    var selectedCity = 0
    var selectedCountiesOfCity = 0
    var selectedDistricts = 0


    private var cityController: CityController? = null
    private var clientAddressController: ClientAddressController? = null
    private var townsController: TownController? = null


    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        addressesNameEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        nameSurnameEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        citySpinner?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        districtSpinner?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        neighborhoodSpinner?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        addressesEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        postCodeEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        phoneEdit?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.grey_color)).strokeWidth(3).cornerRadius(10).build()
        saveButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.white)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(10).build()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addresses_detail)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)
        addressesNameEdit = findViewById(R.id.addressesNameEdit)
        nameSurnameEdit = findViewById(R.id.nameSurnameEdit)
        citySpinner = findViewById(R.id.citySpinner)
        districtSpinner = findViewById(R.id.districtSpinner)
        neighborhoodSpinner = findViewById(R.id.neighborhoodSpinner)
        addressesEdit = findViewById(R.id.addressesEdit)
        postCodeEdit = findViewById(R.id.postCodeEdit)
        phoneEdit = findViewById(R.id.phoneEdit)
        saveButton = findViewById(R.id.saveButton)


        townsController = TownController(this,null)
        townsController?.townsListener = this

        cityController = CityController(this,null)
        cityController?.citiesListener = this
        cityController?.cities("DESC",null,null)


        saveButton?.setOnClickListener {

            if (addressesNameEdit?.text.isNullOrEmpty() || nameSurnameEdit?.text.isNullOrEmpty() || selectedCity == 0 || selectedCountiesOfCity == 0 || selectedDistricts == 0 || addressesEdit?.text.isNullOrEmpty()
                || postCodeEdit?.text.isNullOrEmpty() || phoneEdit?.text.isNullOrEmpty()){

                Toast.makeText(applicationContext,"Lütfen tüm alanları doldurunuz.",Toast.LENGTH_LONG).show()

            }else{

                addressesBody = JsonObject()
                addressesBody.addProperty("name",addressesNameEdit?.text.toString())
                addressesBody.addProperty("receiverName",nameSurnameEdit?.text.toString())

                if (selectedCity != 0){
                    addressesBody.addProperty("city",cityData?.hydraMember?.get(selectedCity-1)?.name)
                }

                if (selectedCountiesOfCity != 0){
                    addressesBody.addProperty("county",countiesOfCityData?.towns?.get(selectedCountiesOfCity-1)?.name)
                }

                if (selectedDistricts != 0){
                    addressesBody.addProperty("district",districtsData?.districts?.get(selectedDistricts-1)?.name)
                }

                addressesBody.addProperty("address",addressesEdit?.text.toString())
                addressesBody.addProperty("zipCode",postCodeEdit?.text.toString())
                addressesBody.addProperty("phone",phoneEdit?.text.toString())

                clientAddressController?.postClientAddresses(appPreferences.token,addressesBody)

            }

        }





        clientAddressController = ClientAddressController(this,null)
        clientAddressController?.clientAddressesListener = this


        citySpinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedCity = position

                if (position == 0){

                    parent?.getChildAt(0)?.spinnerTextView?.setTextColor(resources.getColor(R.color.grey))
                    //var firstText = parent?.getChildAt(0) as TextView
                    //firstText.setTextColor(resources.getColor(R.color.grey))

                }else{

                    cityController?.cityId(cityData?.hydraMember?.get(position-1)?.id.toString())

                }

            }

        }

        districtSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedCountiesOfCity = position

                if (position == 0){

                    parent?.getChildAt(0)?.spinnerTextView?.setTextColor(resources.getColor(R.color.grey))

                }else{

                    townsController?.townsId(countiesOfCityData?.towns?.get(position-1)?.id.toString())

                }

            }

        }

        neighborhoodSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedDistricts = position

                if (position == 0){

                    parent?.getChildAt(0)?.spinnerTextView?.setTextColor(resources.getColor(R.color.grey))

                }

            }

        }


        closeImage?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }

    override fun postClientAddresses(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            println("dogru")

            onBackPressed()



        }else{

            println("yanlıs")

        }

    }

    override fun getClientAddresses(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        TODO("Not yet implemented")
    }

    override fun deleteClientAddresses(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        
    }

    override fun getCities(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            cityData = Gson().fromJson(jsonElement,CitiesResponse::class.java)

            cityNames = ArrayList()
            cityNames?.add("Şehirler")


            countiesOfCityNames = ArrayList()
            countiesOfCityNames?.add("İlçeler")
            var adapterCountiesOfCity = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,countiesOfCityNames!!)
            districtSpinner?.adapter = adapterCountiesOfCity


            districtNames = ArrayList()
            districtNames?.add("mahalleler")
            var adapterNeighborhood = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,districtNames!!)
            neighborhoodSpinner?.adapter = adapterNeighborhood


            for (i in 0..cityData?.hydraMember?.size!!-1){

                cityNames?.add(cityData?.hydraMember?.get(i)?.name.toString())

            }


            var adapter = ArrayAdapter<String>(applicationContext, R.layout.spinner_item, R.id.spinnerTextView, cityNames!!)

            citySpinner?.adapter = adapter



        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getCitiesId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            countiesOfCityData = Gson().fromJson(jsonElement,CitiesIdResponse::class.java)

            countiesOfCityNames = ArrayList()
            countiesOfCityNames?.add("İlçeler")

            if (!countiesOfCityData?.towns.isNullOrEmpty()){

                for (i in 0..countiesOfCityData?.towns?.size!!-1){

                    countiesOfCityNames?.add(countiesOfCityData?.towns?.get(i)?.name.toString())

                }

            }

            var adapter = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,countiesOfCityNames!!)

            districtSpinner?.adapter = adapter


        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

            countiesOfCityNames = ArrayList()
            countiesOfCityNames?.add("İlçeler")

            var adapter = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,countiesOfCityNames!!)

            districtSpinner?.adapter = adapter


        }

    }

    override fun getTownsId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            districtsData = Gson().fromJson(jsonElement,TownIdResponse::class.java)

            districtNames = ArrayList()
            districtNames?.add("mahalleler")

            if (!districtsData?.districts.isNullOrEmpty()){

                for (i in 0..districtsData?.districts?.size!!-1){

                    districtNames?.add(districtsData?.districts?.get(i)?.name.toString())

                }

            }

            var adapter = ArrayAdapter(applicationContext,R.layout.spinner_item,R.id.spinnerTextView,districtNames!!)

            neighborhoodSpinner?.adapter = adapter




        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }


    override fun getTowns(
        response: Boolean?,
        jsonArray: JsonArray?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {

    }



}