package com.company.sindirella.view

import android.content.Context
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.company.sindirella.R
import com.company.sindirella.response.ClientAddress.GetClientAddressResponseHydraMembers
import top.defaults.drawabletoolbox.DrawableBuilder

class AddressesCardView(context: Context) : LinearLayout(context) {

    private var nameText: TextView? = null
    private var addresText: TextView? = null
    private var receiverNameText: TextView? = null
    private var deleteAddresIcon: ImageView? = null
    private var mainLayout: LinearLayout? = null

    var deleteAddresListener: DeleteAddresListener? = null

    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_addresses_card_view,this)

        nameText = view.findViewById(R.id.nameText)
        addresText = view.findViewById(R.id.addresText)
        receiverNameText = view.findViewById(R.id.receiverNameText)
        deleteAddresIcon = view.findViewById(R.id.deleteAddresIcon)
        mainLayout = view.findViewById(R.id.mainLayout)

        setUI()

    }

    fun setUI(){

        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).strokeWidth(3).strokeColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(20).build()
        deleteAddresIcon?.setColorFilter(resources.getColor(R.color.red),PorterDuff.Mode.SRC_ATOP)


    }

    fun setData(data: GetClientAddressResponseHydraMembers?){

        nameText?.text = data?.name
        addresText?.text = data?.address
        receiverNameText?.text = data?.receiverName + " - " + data?.phone

        deleteAddresIcon?.setOnClickListener {

            deleteAddresListener?.deleteAddresId(data?.id)

        }

    }

    fun setDataForPay(data: GetClientAddressResponseHydraMembers?){

        deleteAddresIcon?.visibility = View.GONE

        nameText?.text = data?.name
        addresText?.text = data?.address
        receiverNameText?.text = data?.receiverName + " - " + data?.phone

        deleteAddresIcon?.setOnClickListener {

            deleteAddresListener?.deleteAddresId(data?.id)

        }

    }

    fun clickView(){

        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).strokeWidth(3).strokeColor(ContextCompat.getColor(context,R.color.sindirella_blue)).cornerRadius(20).build()

    }

    interface DeleteAddresListener {
        fun deleteAddresId(id: Int?)
    }


}