package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestRegister
import com.company.sindirella.response.Client.PostRegisterResponse
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class LoginController(private var activity: AppCompatActivity?,private var fragment: Fragment?) {

    var postRegisterListener: onPostRegisterListener? = null

    fun postRegister(body: JsonObject?){

        RequestRegister(activity, fragment, body, object : NetworkResponseListener<JsonElement> {
                override fun onResponseReceived(response: JsonElement) {
                    postRegisterListener?.onPostRegisterListener(true, response, -1, null)
                }

                override fun onEmptyResponse(response: JsonElement?) {
                    postRegisterListener?.onPostRegisterListener(true, null, -1, null)
                }

                override fun onError(failMessage: Int, error: ErrorResponse?) {
                    postRegisterListener?.onPostRegisterListener(false, null, failMessage, error)
                }

            })

    }

    interface onPostRegisterListener {
        fun onPostRegisterListener(responseOk:Boolean,jsonElement: JsonElement?, failMessage: Int, error: ErrorResponse?)
    }


}