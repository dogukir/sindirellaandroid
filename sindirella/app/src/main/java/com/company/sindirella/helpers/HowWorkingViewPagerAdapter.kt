package com.company.sindirella.helpers

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.company.sindirella.R
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class HowWorkingViewPagerAdapter(private var context: Context, private var data: ArrayList<String>) : PagerAdapter() {

    private var imageClothes : ImageView? = null
    private var title : TextView? = null
    private var explanation : TextView? = null
    private var mainLayout : ConstraintLayout? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.how_working_item_view,container,false)

        imageClothes = layout.findViewById(R.id.imageClothes)
        title = layout.findViewById(R.id.titleText)
        explanation = layout.findViewById(R.id.explanation)
        mainLayout = layout.findViewById(R.id.mainLayout)

        explanation?.text = data.get(position)

        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(70).build()

        container.addView(layout)

        return layout
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {

        return data.size
    }


}