package com.company.sindirella.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.response.OrderDetail.GetOrderDetailResponseHydraMember
import top.defaults.drawabletoolbox.DrawableBuilder

class payProductCardView(context: Context): RelativeLayout(context) {


    private var productImage: ImageView? = null
    private var brandText: TextView? = null
    private var productNameText: TextView? = null
    private var cargoText: TextView? = null
    private var priceText: TextView? = null
    private var productLayout: LinearLayout? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_pay_product_card_view,this)

        productImage = view.findViewById(R.id.productImage)
        brandText = view.findViewById(R.id.brandText)
        productNameText = view.findViewById(R.id.productNameText)
        cargoText = view.findViewById(R.id.cargoText)
        priceText = view.findViewById(R.id.priceText)
        productLayout = view.findViewById(R.id.productLayout)

        setUI()

    }

    fun setUI(){

        productLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(40).build()

    }


    fun setData(data: GetOrderDetailResponseHydraMember?){

        if (!data?.product?.pictures.isNullOrEmpty()){

            var requestOptions = RequestOptions()
            requestOptions.transform(CenterCrop(), GranularRoundedCorners(40f,0f,0f,40f))
            Glide.with(context).load(data?.product?.pictures?.get(0)?.imageUrl).apply(requestOptions).into(productImage!!)

        }

        brandText?.text = data?.product?.brand?.name
        priceText?.text = data?.amountTotal.toString() + ".0 ₺"
        productNameText?.text = data?.product?.name






    }



}