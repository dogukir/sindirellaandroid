package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Media.GetMediaIdImgUrlOriSizeResponse
import com.company.sindirella.response.Media.GetMediaIdOriSizeResponse

class RequestGetMediaIdOriSize(activity: AppCompatActivity?, fragment: Fragment?, id : String?, listener: NetworkResponseListener<GetMediaIdOriSizeResponse>) {

    init {
        val request = RequestCreator.create<Service.GetMediaIdOriSize>(Service.GetMediaIdOriSize::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMediaIdOriSize(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}