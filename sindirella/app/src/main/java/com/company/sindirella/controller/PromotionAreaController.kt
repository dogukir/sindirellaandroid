package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestPromotionAreas
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement

class PromotionAreaController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var promotionAreasListener :PromotionAreasListener? = null

    fun promotionAreas(page: Int?){

        RequestPromotionAreas(activity,fragment,page,object: NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                promotionAreasListener?.getPromotionAreas(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                promotionAreasListener?.getPromotionAreas(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                promotionAreasListener?.getPromotionAreas(false,null,failMessage,error)
            }

        })


    }



    interface PromotionAreasListener {
        fun getPromotionAreas(response: Boolean?,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}