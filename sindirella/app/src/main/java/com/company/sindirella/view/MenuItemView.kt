package com.company.sindirella.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.company.sindirella.R

class MenuItemView(context: Context?) : RelativeLayout(context) {

    var itemTextView : TextView? = null
    var sendScreenNameListener: SendScreenNameListener? = null
    var mainLayout: LinearLayout? = null

    init {
        val layout = View.inflate(context,R.layout.menu_items_view,this)

        mainLayout = layout.findViewById(R.id.mainLayout)
        itemTextView = layout.findViewById(R.id.itemTextView)

    }


    fun setTitle(title: String){

        itemTextView?.text = title

        mainLayout?.setOnClickListener {

            sendScreenNameListener?.sendScreenName(title)

        }

    }

    interface SendScreenNameListener {
        fun sendScreenName(screenName: String?)
    }


}