package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.http.Body

class RequestCheckOrder(activity: AppCompatActivity?, fragment: Fragment?, token: String?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PostCheckOrder>(Service.PostCheckOrder::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postCheckOrder(token,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}