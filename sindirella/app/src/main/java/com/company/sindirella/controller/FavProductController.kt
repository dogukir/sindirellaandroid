package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestDelFavProductId
import com.company.sindirella.requestApi.RequestGetFavProduct
import com.company.sindirella.requestApi.RequestPostFavProduct
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class FavProductController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var favProductListener: FavProductListener? = null

    fun getFavProduct(token: String?,page: Int?){

        RequestGetFavProduct(activity,fragment,token,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                favProductListener?.getFavProduct(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                favProductListener?.getFavProduct(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                favProductListener?.getFavProduct(false,null,failMessage,error)
            }

        })

    }


    fun postFavProduct(token: String?,body: JsonObject?){
        RequestPostFavProduct(activity,fragment,token,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                favProductListener?.postFavProduct(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                favProductListener?.postFavProduct(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                favProductListener?.postFavProduct(false,null,failMessage,error)
            }

        })
    }

    fun deleteFavProductId(token: String?,id: String?){

        RequestDelFavProductId(activity,fragment,token,id,object : NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                favProductListener?.deleteFavProductId(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                favProductListener?.deleteFavProductId(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                favProductListener?.deleteFavProductId(false,null,-1,error)
            }

        })

    }


    interface FavProductListener{
        fun getFavProduct(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun postFavProduct(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun deleteFavProductId(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}