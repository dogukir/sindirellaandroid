package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.GetMeResponse
import com.company.sindirella.response.Client.PostLoginResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class RequestGetMe(activity: AppCompatActivity?, fragment: Fragment?,token: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.GetMe>(Service.GetMe::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getMe(token).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}