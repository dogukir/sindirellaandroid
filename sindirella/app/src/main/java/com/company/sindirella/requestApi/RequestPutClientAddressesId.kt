package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.ClientAddress.GetClientAddressIdResponse
import com.company.sindirella.response.ClientAddress.PutClientAddressIdResponse
import com.google.gson.JsonObject

class RequestPutClientAddressesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, body: JsonObject?, listener: NetworkResponseListener<PutClientAddressIdResponse>) {

    init {
        val request = RequestCreator.create<Service.PutClientAddressesId>(Service.PutClientAddressesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.putClientAddressesId(id,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}