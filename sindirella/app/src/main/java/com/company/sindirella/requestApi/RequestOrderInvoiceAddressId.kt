package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.OrderDetail.GetOrderDetailIdResponse
import com.company.sindirella.response.OrderInvoiceAddress.OrderInvoiceAddressIdResponse

class RequestOrderInvoiceAddressId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<OrderInvoiceAddressIdResponse>) {

    init {
        val request = RequestCreator.create<Service.OrderInvoiceAddressId>(Service.OrderInvoiceAddressId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getOrderInvoiceAddressId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}