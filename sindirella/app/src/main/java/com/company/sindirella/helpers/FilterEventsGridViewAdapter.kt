package com.company.sindirella.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import top.defaults.drawabletoolbox.DrawableBuilder
import com.company.sindirella.R
import com.company.sindirella.response.Activity.ActivitysResponseHydraMember


class FilterEventsGridViewAdapter(var context: Context,var eventsData: ArrayList<ActivitysResponseHydraMember>) : BaseAdapter() {

    private var mainLayout: RelativeLayout? = null
    private var boxLayout: RelativeLayout? = null
    private var eventName: TextView? = null
    var selectedArray = ArrayList<Int>()
    var sendSelectedActivityListener: SendSelectedActivityListener? = null

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.filter_events_grid_view,parent,false)

        mainLayout = view.findViewById(R.id.mainLayout)
        boxLayout = view.findViewById(R.id.boxLayout)
        eventName = view.findViewById(R.id.eventName)

        eventName?.text = eventsData.get(position).name?.toUpperCase()


        if (selectedArray.isEmpty()){

            boxLayout?.background = DrawableBuilder().solidColor(context.resources.getColor(R.color.white)).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(30).build()

        }else{

            var control = false

            for (i in 0 until selectedArray.size){

                if (selectedArray.get(i) == position){

                    control = true
                    boxLayout?.background = DrawableBuilder().solidColor(context.resources.getColor(R.color.sindirella_blue)).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(30).build()

                }

            }

            if (!control){

                boxLayout?.background = DrawableBuilder().solidColor(context.resources.getColor(R.color.white)).strokeColor(context.resources.getColor(R.color.black)).strokeWidth(5).cornerRadius(30).build()

            }

        }

        mainLayout?.setOnClickListener {

            if (selectedArray.isEmpty()){

                selectedArray.add(position)

            }else{

                var control = false

                for (i in selectedArray.size-1 downTo 0 step 1){

                    if (selectedArray.get(i) == position){

                        selectedArray.removeAt(i)
                        control = true

                    }

                }

                if (!control){
                    selectedArray.add(position)
                }

            }

            sendSelectedActivityListener?.selectedActivityListener(selectedArray)

            notifyDataSetChanged()

        }




        return view

    }

    override fun getItem(position: Int): Any {
        return eventsData.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return eventsData.size
    }

    interface SendSelectedActivityListener{
        fun selectedActivityListener(selectedArray: java.util.ArrayList<Int>)
    }


}