package com.company.sindirella.response.InvoiceLine

import com.google.gson.annotations.SerializedName

class InvoiceLineResponse {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("invoice")
    var invoice: String? = null

    @SerializedName("orderDetail")
    var orderDetail: String? = null

    @SerializedName("amountNet")
    var amountNet: Int? = null

    @SerializedName("amountVat")
    var amountVat: Int? = null

    @SerializedName("vatRate")
    var vatRate: Int? = null

    @SerializedName("amountTotal")
    var amountTotal: Int? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    init {
        id = null
        invoice = null
        orderDetail = null
        amountNet = null
        amountVat = null
        vatRate = null
        amountTotal = null
        description = null
        deletedAt = null
        deleted = null
        createdAt = null
        updatedAt = null
    }

}