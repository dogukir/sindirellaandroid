package com.company.sindirella.helpers

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.LoginActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.Product.ProductResponseHydraMember
import com.github.twocoffeesoneteam.glidetovectoryou.GlideApp
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class CommentPhotosRecyclerAdapter(private val context: Context?, private var photos: ArrayList<Uri>) : RecyclerView.Adapter<CommentPhotosRecyclerAdapter.ViewHolder>() {


    private var imageView: ImageView? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.comment_photos_recyclerview, parent, false)

        imageView = v.findViewById<ImageView>(R.id.imageView)





        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        //imageView?.setImageURI(photos.get(position))
        val requestOptions = RequestOptions()
        requestOptions.transform(CenterCrop(),GranularRoundedCorners(30f,30f,30f,30f))

        GlideApp.with(context!!).load(photos.get(position)).apply(requestOptions).into(imageView!!)


        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {

        return photos.size

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val button: CustomCheckBox = itemView.findViewById(R.id.radioButton)
        //val text:AppTextView = itemView.findViewById(R.id.textLabel)

    }

}