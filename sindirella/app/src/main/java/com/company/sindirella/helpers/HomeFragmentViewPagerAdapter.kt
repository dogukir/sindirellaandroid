package com.company.sindirella.helpers

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.fragments.BrowseFragment
import com.company.sindirella.response.PromotionArea.PromotionAreaResponse
import com.google.gson.Gson
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class HomeFragmentViewPagerAdapter(private var context: Context, var activity: MainActivity?, private var data: PromotionAreaResponse) : PagerAdapter() {

    private var imageViewClothes : ImageView? = null
    private var titleView : TextView? = null
    private var explanationView : TextView? = null
    private var browseButton : Button? = null
    private var pagerAdapterLayout : LinearLayout? = null

    var promotionClick : String? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.mainpage_browse_view,container,false)

        imageViewClothes = layout.findViewById(R.id.imageViewClothes)
        titleView = layout.findViewById(R.id.titleView)
        explanationView = layout.findViewById(R.id.explanationView)
        browseButton = layout.findViewById(R.id.browseButton)
        pagerAdapterLayout = layout.findViewById(R.id.pagerAdapterLayout)


        titleView?.text = data.hydraMember?.get(position)?.title
        explanationView?.text = data.hydraMember?.get(position)?.summary

        var requestOptions = RequestOptions()
        requestOptions.transform(FitCenter(), RoundedCorners(20))


        Glide.with(context).load(data.hydraMember?.get(position)?.fullImage).apply(requestOptions).into(imageViewClothes!!)

        browseButton?.text = data.hydraMember?.get(position)?.buttonTitle


        if (position == 0){
            promotionClick = data.hydraMember?.get(position)?.apiUrl?.substring(1,data.hydraMember?.get(position)?.apiUrl?.length!!)!!
        }

        pagerAdapterLayout?.setOnClickListener {

            promotionClick = data.hydraMember?.get(position)?.apiUrl?.substring(1,data.hydraMember?.get(position)?.apiUrl?.length!!)!!

            var browsFrag = BrowseFragment(activity)
            val bundle = Bundle()
            bundle.putString("promotionClick", promotionClick)
            browsFrag.arguments = bundle
            activity?.changeFragment(browsFrag)
            activity?.navigationView?.menu?.getItem(1)?.isChecked = true

        }

        browseButton?.setOnClickListener {

            promotionClick = data.hydraMember?.get(position)?.apiUrl?.substring(1,data.hydraMember?.get(position)?.apiUrl?.length!!)!!

            var browsFrag = BrowseFragment(activity)
            val bundle = Bundle()
            bundle.putString("promotionClick", promotionClick)
            browsFrag.arguments = bundle
            activity?.changeFragment(browsFrag)
            activity?.navigationView?.menu?.getItem(1)?.isChecked = true

        }

        setUI()


        container.addView(layout)

        return layout
    }

    fun setUI(){

        //imageViewClothes?.background = DrawableBuilder().cornerRadius(50).build()
        browseButton?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(context,R.color.white)).strokeWidth(4).cornerRadius(20).build()
        pagerAdapterLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(20).build()

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {

        return data.hydraMember?.size!!
    }


}