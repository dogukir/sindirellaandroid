package com.company.sindirella.response.Category

import com.google.gson.annotations.SerializedName

class CategoriesIdResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("slug")
    var slug : String? = null

    init {
        id = null
        name = null
        slug = null
    }

}