package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Client.PostGoogleResponse
import com.company.sindirella.response.Client.PostLoginResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class RequestLogin(activity: AppCompatActivity?, fragment: Fragment?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.Login>(Service.Login::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postLogin(body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}