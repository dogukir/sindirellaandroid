package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Category.CategoriesIdResponse
import com.company.sindirella.response.Category.CategoriesResponse
import java.util.ArrayList

class RequestCategoriesId(activity: AppCompatActivity?, fragment: Fragment?,id: String?, listener: NetworkResponseListener<CategoriesIdResponse>) {

    init {
        val request = RequestCreator.create<Service.CategoriesId>(Service.CategoriesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCategoriesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}