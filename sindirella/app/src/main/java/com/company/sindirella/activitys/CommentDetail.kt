package com.company.sindirella.activitys

import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.helpers.CommentDetailViewPagerAdapter
import com.company.sindirella.response.Comment.GetCommentsResponse
import com.company.sindirella.response.Comment.GetCommentsResponseHydraMembers
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class CommentDetail :AppCompatActivity() {

    private var viewPager : ViewPager? = null
    private var adapter : CommentDetailViewPagerAdapter? = null
    private var backPressedText : TextView? = null

    private var commentCardLayout: LinearLayout? = null
    private var titleText: TextView? = null
    private var commentCloseIcon: ImageView? = null
    private var dateText: TextView? = null
    private var commentTitle: TextView? = null
    private var commentDescription: TextView? = null
    private var closeIcon: ImageView? = null
    private var commentRatingStar: RatingBar? = null

    var commentData: GetCommentsResponseHydraMembers? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_detail)
        supportActionBar?.hide()

        commentData = intent.getSerializableExtra("obj") as GetCommentsResponseHydraMembers

        viewPager = findViewById(R.id.viewPager)
        backPressedText = findViewById(R.id.backPressedText)


        commentCardLayout = findViewById(R.id.commentCardLayout)
        titleText = findViewById(R.id.titleText)
        commentCloseIcon = findViewById(R.id.commentCloseIcon)
        dateText = findViewById(R.id.dateText)
        commentTitle = findViewById(R.id.commentTitle)
        commentDescription = findViewById(R.id.commentDescription)
        closeIcon = findViewById(R.id.closeIcon)
        commentRatingStar = findViewById(R.id.commentRatingStar)


        if (commentData != null){

            if (commentData?.client?.name != null){
                titleText?.text = commentData?.client?.name.toString()
            }
            dateText?.text = commentData?.formattedDate
            commentTitle?.text = commentData?.title
            commentDescription?.text = commentData?.body
            commentRatingStar?.rating = commentData?.rating!!.toFloat()

            adapter = CommentDetailViewPagerAdapter(this,commentData?.medias!!)

            viewPager?.adapter = adapter
        }

        commentCardLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(applicationContext,R.color.black_blur)).cornerRadius(30).build()
        var draw = resources.getDrawable(R.drawable.heightt)
        draw.setColorFilter(resources.getColor(R.color.white),PorterDuff.Mode.SRC_ATOP)
        //commentCloseIcon?.background = draw



        backPressedText?.setOnClickListener {

            onBackPressed()

        }

        commentCloseIcon?.setOnClickListener {

            onBackPressed()

        }

        closeIcon?.setOnClickListener {

            onBackPressed()

        }


        setUI()

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }


}