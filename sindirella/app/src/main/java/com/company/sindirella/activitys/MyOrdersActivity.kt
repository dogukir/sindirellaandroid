package com.company.sindirella.activitys

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.OrderController
import com.company.sindirella.helpers.MyOrdersRecyclerAdapter
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Order.ActiveOrdersResponse
import com.google.gson.Gson
import com.google.gson.JsonElement

class MyOrdersActivity: AppCompatActivity(),OrderController.ActiveOrdersListener,MyOrdersRecyclerAdapter.MyOrderClickListener {

    private var closeIcon: ImageView? = null
    private var myOrderTitleText: TextView? = null
    private var recyclerView: RecyclerView? = null


    private var orderController: OrderController? = null


    private var myOrdersRecyclerAdapter: MyOrdersRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders)
        supportActionBar?.hide()

        closeIcon = findViewById(R.id.closeIcon)
        myOrderTitleText = findViewById(R.id.myOrderTitleText)
        recyclerView = findViewById(R.id.recyclerView)

        orderController = OrderController(this,null)
        orderController?.activeOrdersListener = this
        orderController?.getActiveOrders(appPreferences.token,null,null,null,null,null,null)




        closeIcon?.setOnClickListener {

            onBackPressed()

        }

        setUI()


    }

    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }

    override fun getActiveOrders(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var activeOrdersData = Gson().fromJson(jsonElement,ActiveOrdersResponse::class.java)

            if (!activeOrdersData.hydraMember.isNullOrEmpty()){

                myOrdersRecyclerAdapter = MyOrdersRecyclerAdapter(applicationContext,activeOrdersData.hydraMember?.get(0)?.orderDetails!!,activeOrdersData)
                myOrdersRecyclerAdapter?.myOrderClickListener = this

                recyclerView?.adapter = myOrdersRecyclerAdapter

                val layoutManager = LinearLayoutManager(applicationContext,LinearLayoutManager.VERTICAL,false)

                recyclerView?.layoutManager = layoutManager
                recyclerView?.itemAnimator = DefaultItemAnimator()

            }


        }else{

            Toast.makeText(applicationContext,error?.hydraDescription, Toast.LENGTH_LONG).show()

        }

    }

    override fun myOrderClick(id: Int?) {

        val intent = Intent(applicationContext,OrderSummaryActivity::class.java)
        intent.putExtra("id",id)
        startActivity(intent)

    }




}