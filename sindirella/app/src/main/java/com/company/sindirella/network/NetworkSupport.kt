package com.company.sindirella.network

interface NetworkSupport {

    interface NetworkAdress {
        companion object{
            val base_url = "https://api.sindirella.com/api/"
        }
    }

    interface HttpCodes {
        companion object{
            val OK = 200
            val CONFLICT = 409
            val CREATED = 201
            val NOCONTENT = 204
        }
    }

}