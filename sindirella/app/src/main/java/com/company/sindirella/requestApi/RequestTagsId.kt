package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Tag.TagIdResponse
import com.company.sindirella.response.Tag.TagResponse
import java.util.ArrayList

class RequestTagsId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<TagIdResponse>) {

    init {
        val request = RequestCreator.create<Service.TagsId>(Service.TagsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getTagsId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}