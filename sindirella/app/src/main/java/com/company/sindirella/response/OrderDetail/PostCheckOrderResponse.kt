package com.company.sindirella.response.OrderDetail

import com.google.gson.annotations.SerializedName

class PostCheckOrderResponse {

    @SerializedName("response")
    var response: String? = null

}