package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Order.GetOrdersIdResponse
import com.company.sindirella.response.Order.PutOrdersIdResponse
import com.google.gson.JsonObject

class RequestPutOrdersId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, body: JsonObject?, listener: NetworkResponseListener<PutOrdersIdResponse>) {

    init {
        val request = RequestCreator.create<Service.PutOrdersId>(Service.PutOrdersId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.putOrdersId(id,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}