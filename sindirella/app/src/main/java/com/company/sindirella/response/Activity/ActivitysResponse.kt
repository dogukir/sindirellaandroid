package com.company.sindirella.response.Activity

import com.company.sindirella.response.PromotionArea.PromotionAreaResponseHydraMember
import com.company.sindirella.response.PromotionArea.PromotionAreaResponseHydraSearch
import com.company.sindirella.response.PromotionArea.PromotionAreaResponseHydraSearchMapping
import com.company.sindirella.response.PromotionArea.PromotionAreaResponseHydraView
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ActivitysResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<ActivitysResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

    @SerializedName("hydra:view")
    var hydraView: ActivitysResponseHydraView? = null

    @SerializedName("hydra:search")
    var hydraSearch: ActivitysResponseHydraSearch? = null

}

class ActivitysResponseHydraMember {

    @SerializedName("@id")
    var BigId : String? = null

    @SerializedName("@type")
    var BigType : String? = null

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("imageCode")
    var imageCode : String? = null

    @SerializedName("slug")
    var slug : String? = null

    @SerializedName("imageUrl")
    var imageUrl : String? = null

    init {

        id = null
        name = null
        imageCode = null
        slug = null
        imageUrl = null

    }

}

class ActivitysResponseHydraView {

    @SerializedName("hydra:first")
    var hydraFirst: String? = null

    @SerializedName("hydra:last")
    var hydraLast: String? = null

    @SerializedName("hydra:next")
    var hydraNext: String? = null

    init {
        hydraFirst = null
        hydraLast = null
        hydraNext = null
    }

}

class ActivitysResponseHydraSearch {

    @SerializedName("hydra:template")
    var hydraTemplate: String? = null

    @SerializedName("hydra:variableRepresentation")
    var hydraVariableRepresentation: String? = null

    @SerializedName("hydra:mapping")
    var hydraMapping: ArrayList<ActivitysResponseHydraSearchMapping>? = null

}

class ActivitysResponseHydraSearchMapping {

    @SerializedName("variable")
    var variable: String? = null

    @SerializedName("property")
    var property: String? = null

    @SerializedName("required")
    var required: Boolean? = null

}