package com.company.sindirella.response.Color

import com.google.gson.annotations.SerializedName

class ColorsIdResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("hex")
    var hex : String? = null

    init {
        id = null
        name = null
        hex = null
    }

}