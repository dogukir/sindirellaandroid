package com.company.sindirella.response.Client

import com.google.gson.annotations.SerializedName

class PutPasswordResponse {

    @SerializedName("password")
    var password : String? = null

    @SerializedName("retypedPassword")
    var retypedPassword : String? = null

    init {
        password = null
        retypedPassword = null
    }

}