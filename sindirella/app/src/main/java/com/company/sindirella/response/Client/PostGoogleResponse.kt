package com.company.sindirella.response.Client

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class PostGoogleResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("email")
    var email : String? = null

    @SerializedName("phone")
    var phone : String? = null

    @SerializedName("socialFbId")
    var socialFbId : String? = null

    @SerializedName("socialGoogleId")
    var socialGoogleId : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("birthday")
    var birthday : String? = null

    @SerializedName("lastLogin")
    var lastLogin : String? = null

    @SerializedName("extra")
    var extra : ArrayList<String>? = null

    @SerializedName("city")
    var city : String? = null

    @SerializedName("size")
    var size : String? = null

    @SerializedName("country")
    var country : String? = null

    init {
        id = null
        email = null
        phone = null
        socialFbId = null
        socialGoogleId = null
        name = null
        birthday = null
        lastLogin = null
        extra = null
        city = null
        size = null
        country = null
    }

}