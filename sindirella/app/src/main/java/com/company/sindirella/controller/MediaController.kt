package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestGetMedia
import com.company.sindirella.requestApi.RequestPostMedia
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart

class MediaController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var mediaListener: MediaListener? = null

    fun postMedia(token: String,imageFile : MultipartBody.Part){
        RequestPostMedia(activity,fragment,token,imageFile,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                mediaListener?.postMedia(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                mediaListener?.postMedia(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                mediaListener?.postMedia(false,null,failMessage,error)
            }

        })
    }



    fun getMedia(page: Int?){
        RequestGetMedia(activity,fragment,page,object : NetworkResponseListener<JsonArray>{
            override fun onResponseReceived(response: JsonArray) {
                mediaListener?.getMedia(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonArray?) {
                mediaListener?.getMedia(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                mediaListener?.getMedia(false,null,failMessage,error)
            }

        })
    }


    interface MediaListener{
        fun postMedia(response: Boolean?,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun getMedia(response: Boolean?,jsonArray: JsonArray?,failMessage: Int?,error: ErrorResponse?)
    }



}