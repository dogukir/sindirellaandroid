package com.company.sindirella.activitys

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.*
import com.company.sindirella.helpers.ProductDetailViewPagerAdapter
import com.company.sindirella.response.City.CitiesResponse
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.OrderDetail.PostCheckOrderResponse
import com.company.sindirella.response.Product.ProductDetailResponse
import com.company.sindirella.response.Product.ProductIdResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import top.defaults.drawabletoolbox.DrawableBuilder
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.YearMonth
import java.util.*
import java.util.concurrent.TimeoutException
import kotlin.collections.ArrayList

class ProductDetailActivity : AppCompatActivity(), ProductController.ProductListener,DynamicApiController.DynamicApiListener, OrderDetailController.OrderDetailsListener, FavProductController.FavProductListener, CityController.CitiesListener {


    private var backIcon : TextView? = null
    private var shareIcon: ImageView? = null
    private var likeIcon: ImageView? = null
    private var pageTitle: TextView? = null
    private var brandName: TextView? = null
    private var productName: TextView? = null
    private var viewPager: ViewPager? = null
    private var sizeTitle: TextView? = null
    private var spinnerSize: Spinner? = null
    private var whichDaysTitle: TextView? = null
    private var startDateText: TextView? = null
    private var endDateText: TextView? = null
    private var productDetailTitle: TextView? = null
    private var productDescription: TextView? = null
    private var productSizeTitle: TextView? = null
    private var productSize: TextView? = null
    private var productColorTitle: TextView? = null
    private var productColor: TextView? = null
    private var productRentTitle: TextView? = null
    private var productRent: TextView? = null
    private var productSalesTitle: TextView? = null
    private var productSales: TextView? = null
    private var productCategoriesTitle: TextView? = null
    private var productCategories: TextView? = null
    private var productEventsTitle: TextView? = null
    private var productEvents: TextView? = null
    private var stylistNoteTitle: TextView? = null
    private var stylistNote: TextView? = null
    private var commentSize: TextView? = null
    private var openCommentButton: Button? = null
    private var returnConditionsButton: Button? = null
    private var confidentialityAgreementButton: Button? = null
    private var pricesOld: TextView? = null
    private var pricesNew: TextView? = null
    private var addBasketButton: Button? = null
    private var webView: WebView? = null
    private var starSizeText: TextView? = null
    private var favData: GetFavProductResponse? = null
    private var brandSpinnerLayout: RelativeLayout? = null
    private var citySpinnerLayout: RelativeLayout? = null
    private var spinnerCity: Spinner? = null
    private var daysSpinnerLayout: RelativeLayout? = null
    private var spinnerDays: Spinner? = null
    private var productRatingStar: RatingBar? = null
    private var postBody: JsonObject? = null
    private var fourDaysText: TextView? = null
    private var divider: View? = null
    private var sevenDaysText: TextView? = null
    private var cityData: CitiesResponse? = null
    private var cityNames: ArrayList<String>? = null
    private var days: ArrayList<String>? = null
    private var selectedDaysName: String? = null
    private var selectedCityId: Int? = null
    private var datePickerDialog: DatePickerDialog? = null
    private var changeSelectedDay = false

    //private var sdf: SimpleDateFormat? = null

    private var productDetailViewPagerAdapter: ProductDetailViewPagerAdapter? = null

    var productId: String? = null
    var productIdUrl: String? = null
    var productShareUrl:String? = null
    var favDataId: Int? = null


    private var productDetail: ProductDetailResponse? = null

    private var spinnerSizeSelected = -1
    var inventorySizes = ArrayList<String>()


    private var orderDetailController: OrderDetailController? = null
    private var dynamicApiController: DynamicApiController? = null
    private var favProductController: FavProductController? = null
    private var cityController: CityController? = null


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        supportActionBar?.hide()



        backIcon = findViewById(R.id.backIcon)
        shareIcon = findViewById(R.id.shareIcon)
        likeIcon = findViewById(R.id.likeIcon)
        pageTitle = findViewById(R.id.pageTitle)
        brandName = findViewById(R.id.brandName)
        productName = findViewById(R.id.productName)
        starSizeText = findViewById(R.id.starSizeText)
        viewPager = findViewById(R.id.viewPager)
        sizeTitle = findViewById(R.id.sizeTitle)
        spinnerSize = findViewById(R.id.spinnerSize)
        whichDaysTitle = findViewById(R.id.whichDaysTitle)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        productDetailTitle = findViewById(R.id.productDetailTitle)
        productDescription = findViewById(R.id.productDescription)
        productSizeTitle = findViewById(R.id.productSizeTitle)
        productSize = findViewById(R.id.productSize)
        productColorTitle = findViewById(R.id.productColorTitle)
        productColor = findViewById(R.id.productColor)
        productRentTitle = findViewById(R.id.productRentTitle)
        productRent = findViewById(R.id.productRent)
        productSalesTitle = findViewById(R.id.productSalesTitle)
        productSales = findViewById(R.id.productSales)
        productCategoriesTitle = findViewById(R.id.productCategoriesTitle)
        productCategories = findViewById(R.id.productCategories)
        productEventsTitle = findViewById(R.id.productEventsTitle)
        productEvents = findViewById(R.id.productEvents)
        stylistNoteTitle = findViewById(R.id.stylistNoteTitle)
        stylistNote = findViewById(R.id.stylistNote)
        commentSize = findViewById(R.id.commentSize)
        openCommentButton = findViewById(R.id.openCommentButton)
        returnConditionsButton = findViewById(R.id.returnConditionsButton)
        confidentialityAgreementButton = findViewById(R.id.confidentialityAgreementButton)
        pricesOld = findViewById(R.id.pricesOld)
        pricesNew = findViewById(R.id.pricesNew)
        addBasketButton = findViewById(R.id.addBasketButton)
        webView = findViewById(R.id.webView)
        brandSpinnerLayout = findViewById(R.id.brandSpinnerLayout)
        citySpinnerLayout = findViewById(R.id.citySpinnerLayout)
        spinnerCity = findViewById(R.id.spinnerCity)
        daysSpinnerLayout = findViewById(R.id.daysSpinnerLayout)
        spinnerDays = findViewById(R.id.spinnerDays)
        productRatingStar = findViewById(R.id.productRatingStar)
        fourDaysText = findViewById(R.id.fourDaysText)
        divider = findViewById(R.id.divider)
        sevenDaysText = findViewById(R.id.sevenDaysText)


        productId = intent.getStringExtra("id")


        favProductController = FavProductController(this,null)
        favProductController?.favProductListener = this
        if (!appPreferences.token.equals("")){
            favProductController?.getFavProduct(appPreferences.token,null)
        }else{

            if (productId != null){

                dynamicApiController = DynamicApiController(this,null)
                dynamicApiController?.dynamicApiListener = this
                dynamicApiController?.dynamicApi(productId.toString(),null,null,null,null,null)

                //productController = ProductController(this,null)
                //productController?.productListener = this
                //productController?.productId(productId)

            }

        }


        orderDetailController = OrderDetailController(this,null)
        orderDetailController?.orderDetailsListener = this

        cityController = CityController(this,null)
        cityController?.citiesListener = this
        cityController?.cities("DESC",null,null)


        shareIcon?.setOnClickListener {

            if(!this.productShareUrl.isNullOrEmpty()) {
                val intent = Intent(Intent.ACTION_SEND)
                intent.setType("text/plain")
                intent.putExtra(Intent.EXTRA_SUBJECT,this.productName?.text)
                intent.putExtra(Intent.EXTRA_TEXT, this.productShareUrl)

                startActivity(Intent.createChooser(intent, "Paylaş"))
            }


        }

        returnConditionsButton?.setOnClickListener {

            val intent = Intent(this,MenuWebViewActivity::class.java)
            intent.putExtra("code","cayma-hakki")
            startActivity(intent)

        }

        confidentialityAgreementButton?.setOnClickListener {

            val intent = Intent(this,MenuWebViewActivity::class.java)
            intent.putExtra("code","gizlilik-sozlesmesi")
            startActivity(intent)

        }

        backIcon?.setOnClickListener {

            onBackPressed()

        }




        var arr = ArrayList<String>()

        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")
        arr.add("dogu")

        //adapter = ProductDetailViewPagerAdapter(this,arr)
        //viewPager?.adapter = adapter








        var controllVal = 0

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ROOT)
        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        var startCalendarBool = false
        var endCalendarBool = false

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            if (controllVal == 1) {

                changeSelectedDay = false

                startCalendarBool = true

                calendarStart.set(Calendar.YEAR, year)
                calendarStart.set(Calendar.MONTH, monthOfYear)
                calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                startDateText?.text = sdf.format(calendarStart.time)
                startDateText?.setTextColor(resources.getColor(R.color.black))

                var mycal = GregorianCalendar(year, monthOfYear, dayOfMonth)
                var daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH)
                daysInMonth

                if (selectedDaysName.equals("4 gün")){
                    // 4 gün

                    daysInMonth = daysInMonth - 4
                    if (dayOfMonth <= daysInMonth){

                        calendarEnd.set(Calendar.YEAR, year)
                        calendarEnd.set(Calendar.MONTH, monthOfYear)
                        var newDay = dayOfMonth + 4
                        calendarEnd.set(Calendar.DAY_OF_MONTH, newDay)

                        endDateText?.text = sdf.format(calendarEnd.time)
                        endDateText?.setTextColor(resources.getColor(R.color.black))

                    }else{

                        daysInMonth = daysInMonth + 4
                        var different = daysInMonth - dayOfMonth
                        var asd = 4 - different

                        calendarEnd.set(Calendar.YEAR, year)
                        var newMonth = monthOfYear + 1
                        calendarEnd.set(Calendar.MONTH, newMonth)
                        calendarEnd.set(Calendar.DAY_OF_MONTH, asd)

                        endDateText?.text = sdf.format(calendarEnd.time)
                        endDateText?.setTextColor(resources.getColor(R.color.black))
                    }

                }else if (selectedDaysName.equals("7 gün")){
                    // 7 gün

                    daysInMonth = daysInMonth - 7
                    if (dayOfMonth <= daysInMonth){

                        calendarEnd.set(Calendar.YEAR, year)
                        calendarEnd.set(Calendar.MONTH, monthOfYear)
                        var newDay = dayOfMonth + 7
                        calendarEnd.set(Calendar.DAY_OF_MONTH, newDay)

                        endDateText?.text = sdf.format(calendarEnd.time)
                        endDateText?.setTextColor(resources.getColor(R.color.black))

                    }else{

                        daysInMonth = daysInMonth + 7
                        var different = daysInMonth - dayOfMonth
                        var asd = 4 - different

                        calendarEnd.set(Calendar.YEAR, year)
                        var newMonth = monthOfYear + 1
                        calendarEnd.set(Calendar.MONTH, newMonth)
                        calendarEnd.set(Calendar.DAY_OF_MONTH, asd)

                        endDateText?.text = sdf.format(calendarEnd.time)
                        endDateText?.setTextColor(resources.getColor(R.color.black))
                    }

                }

                /*calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)
                endDateText?.setTextColor(resources.getColor(R.color.black))

                 */

            } else {
                endCalendarBool = true

                calendarEnd.set(Calendar.YEAR, year)
                calendarEnd.set(Calendar.MONTH, monthOfYear)
                calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                endDateText?.text = sdf.format(calendarEnd.time)
                endDateText?.setTextColor(resources.getColor(R.color.black))

            }
        }

        startDateText?.setOnClickListener {
            controllVal = 1
            datePickerDialog = DatePickerDialog(this, R.style.DatePickerTheme, date, calendarStart.get(Calendar.YEAR), calendarStart.get(
                Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH))
            datePickerDialog?.show()

        }

        endDateText?.setOnClickListener {
            controllVal = 2
            DatePickerDialog(this, R.style.DatePickerTheme, date, calendarEnd.get(Calendar.YEAR), calendarEnd.get(
                Calendar.MONTH), calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }

        openCommentButton?.setOnClickListener {

            val intent = Intent(this,CommentsActivity::class.java)
            intent.putExtra("productId",productId)
            intent.putExtra("brandName",brandName?.text)
            intent.putExtra("productName",productName?.text)
            intent.putExtra("productIdUrl",productIdUrl)
            startActivity(intent)

        }

        likeIcon?.setOnClickListener {

            if (appPreferences.token.equals("")){
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
            }else{

                productId

                var likeControl = false

                if (favData != null){

                    for (i in 0 until favData?.hydraMember?.size!!){

                        if (productId.equals(favData?.hydraMember?.get(i)?.product?.BigId)){

                            likeControl = true
                            favDataId = favData?.hydraMember?.get(i)?.id

                        }

                    }

                }

                if (likeControl){

                    favProductController?.deleteFavProductId(appPreferences.token,favDataId.toString())

                }else{

                    var likeBody = JsonObject()



                    likeBody.addProperty("product",productId)
                    likeBody.addProperty("status",1)

                    favProductController?.postFavProduct(appPreferences.token,likeBody)

                }



            }

        }

        addBasketButton?.setOnClickListener {


            if (appPreferences.token.equals("")){
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
            }else{

                if (changeSelectedDay){

                    if (spinnerSizeSelected == -1 || startCalendarBool == false){ // || endCalendarBool == false

                        Toast.makeText(applicationContext,"Lütfen Beden ve Tarih Alanlarını Doldurunuz",Toast.LENGTH_LONG).show()

                    }else if(spinnerSizeSelected == -1 && startCalendarBool == true){

                        Toast.makeText(applicationContext,"Lütfen Beden Seçimi Yapınız",Toast.LENGTH_LONG).show()

                    }else if (spinnerSizeSelected != -1 && startCalendarBool == false){

                        Toast.makeText(applicationContext,"Lütfen Tarih Alanlanını Doldurunuz",Toast.LENGTH_LONG).show()

                    }else{

                        Toast.makeText(applicationContext,"Gün değişimi yaptınız. Lütfen Tekrar Tarih Seçiniz",Toast.LENGTH_LONG).show()

                    }

                }else{

                    var body = JsonObject()

                    body.addProperty("amountTotal",productDetail?.priceRental)
                    body.addProperty("product",productDetail?.BigId)

                    if (spinnerSizeSelected == -1 || startCalendarBool == false){ // || endCalendarBool == false
                        Toast.makeText(applicationContext,"Lütfen Beden ve Tarih Alanlarını Doldurunuz",Toast.LENGTH_LONG).show()
                    }else{

                        if (selectedDaysName == null){

                            Toast.makeText(applicationContext,"Lütfen Gün Seçiniz",Toast.LENGTH_LONG).show()

                        }else{

                            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ROOT)
                            val formattedDateStart = sdf.format(calendarStart.time)
                            val formattedDateEnd = sdf.format(calendarEnd.time)

                            body.addProperty("fromDate",formattedDateStart)
                            body.addProperty("toDate",formattedDateEnd)
                            body.addProperty("sizeGroup","/api/inventories/${productDetail?.inventories?.get(spinnerSizeSelected-1)?.sizeGroup?.id}")

                            postBody = JsonObject()

                            postBody = body

                            orderDetailController?.postCheckOrder(appPreferences.token,body)

                        }



                    }

                }

            }

        }



        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        //commentButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.black)).cornerRadius(50).build()
        returnConditionsButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(20).build()
        confidentialityAgreementButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(20).build()
        addBasketButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.sindirella_blue)).cornerRadius(20).build()
        openCommentButton?.background = DrawableBuilder().solidColor(ContextCompat.getColor(this,R.color.black)).cornerRadius(20).build()
        brandSpinnerLayout?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        citySpinnerLayout?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        daysSpinnerLayout?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        startDateText?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()
        endDateText?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(30).build()

        //startDateText?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()
        //endDateView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()
        //spinnerView?.background = DrawableBuilder().solidColor(resources.getColor(R.color.view_background)).cornerRadius(50).build()

    }

    override fun getFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            favData = Gson().fromJson(jsonElement, GetFavProductResponse::class.java)

            if (!favData?.hydraMember.isNullOrEmpty()){

                for (i in 0 until favData?.hydraMember?.size!!){

                    if (productId.equals(favData?.hydraMember?.get(i)?.product?.BigId)){

                        likeIcon?.background = resources.getDrawable(R.drawable.heart)

                    }

                }


            }else{


            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

        if (productId != null){

            dynamicApiController = DynamicApiController(this,null)
            dynamicApiController?.dynamicApiListener = this
            dynamicApiController?.dynamicApi(productId.toString(),null,null,null,null,null)

            //productController = ProductController(this,null)
            //productController?.productListener = this
            //productController?.productId(productId)

        }

    }

    override fun postFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            likeIcon?.background = resources.getDrawable(R.drawable.heart_like_otuziki)

            favProductController?.getFavProduct(appPreferences.token,null)

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun deleteFavProductId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            likeIcon?.background = resources.getDrawable(R.drawable.heart_dislike_otuziki)

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }



    }



    @RequiresApi(Build.VERSION_CODES.N)
    override fun getDynamicApi(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                productDetail = Gson().fromJson(jsonElement,ProductDetailResponse::class.java)
                productShareUrl = productDetail?.fullLink
                productId = productDetail?.BigId
                productIdUrl = productDetail?.BigId

                brandName?.text = productDetail?.brand?.name
                productName?.text = productDetail?.name
                starSizeText?.text = "(${productDetail?.comments?.size.toString()})"

                productDetailViewPagerAdapter = ProductDetailViewPagerAdapter(applicationContext,productDetail?.pictures)
                viewPager?.adapter = productDetailViewPagerAdapter

                viewPager?.pageMargin = 30
                viewPager?.setPadding(50,0,50,0)
                viewPager?.clipToPadding = false
                viewPager?.offscreenPageLimit = 0


                webView?.settings?.loadWithOverviewMode = true
                webView?.settings?.builtInZoomControls = false
                webView?.isScrollContainer = true
                webView?.settings?.pluginState = WebSettings.PluginState.ON
                webView?.webChromeClient = WebChromeClient()
                webView?.settings?.mediaPlaybackRequiresUserGesture = true

                inventorySizes.add("Beden")

                for (i in 0..productDetail?.inventories?.size!!-1){

                    if (inventorySizes.isNullOrEmpty()){

                        inventorySizes.add(productDetail?.inventories?.get(i)?.sizeGroup?.name.toString())

                    }else if (inventorySizes.size > 0){

                        var invertoryControl = false

                        for (j in 0..inventorySizes.size-1){

                            if (productDetail?.inventories?.get(i)?.sizeGroup?.name.equals(inventorySizes.get(j))){

                                invertoryControl = true

                            }

                        }

                        if (invertoryControl != true){

                            inventorySizes.add(productDetail?.inventories?.get(i)?.sizeGroup?.name.toString())

                        }

                    }

                }

                var adapter = ArrayAdapter<String>(applicationContext, R.layout.spinner_item, R.id.spinnerTextView, inventorySizes)
                spinnerSize?.adapter = adapter

                spinnerSize?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                        if (position != 0){

                            changeSelectedDay = false
                            spinnerSizeSelected = position

                        }else{

                            spinnerSizeSelected = -1

                        }

                    }

                }



                productDescription?.text = Html.fromHtml(productDetail?.description,Html.FROM_HTML_MODE_COMPACT)
                stylistNote?.text = Html.fromHtml(productDetail?.notes,Html.FROM_HTML_MODE_COMPACT)

                //webView?.loadDataWithBaseURL(null,productDetail.description,"text/html","UTF-8",null)
                //webView?.setBackgroundColor(resources.getColor(R.color.view_color))

                productRent?.text = productDetail?.priceRental.toString() + " ₺"
                productSales?.text = productDetail?.priceSale.toString() + " ₺"

                pricesNew?.text = productDetail?.priceRental.toString() + " ₺"
                pricesOld?.text = productDetail?.priceSale.toString() + " ₺"
                pricesOld?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

                commentSize?.text = productDetail?.comments?.size.toString() + " yorum"


                var sizeName = ""
                for (i in 0..productDetail?.inventories?.size!!-1){

                    if (productDetail?.inventories?.get(i)?.sizeGroup != null){

                        sizeName = sizeName + productDetail?.inventories?.get(i)?.sizeGroup?.name + " "

                    }

                }
                productSize?.text = sizeName


                productColor?.text = productDetail?.color?.name


                var categories = ""
                for (i in 0..productDetail?.categories?.size!!-1){

                    categories = "${categories}${productDetail?.categories?.get(i)?.name} \n"

                }
                productCategories?.text = categories

                var events = ""
                for (i in 0..productDetail?.activities?.size!!-1){

                    events = events + productDetail?.activities?.get(i)?.name + "\n"

                }
                productEvents?.text = events

                days = ArrayList()
                //days?.add("Gün")

                if (productDetail?.priceRental != null){
                    days?.add("4 gün")
                    fourDaysText?.text = "4 gün: " + productDetail?.priceRental?.toFloat().toString() + " ₺"
                }else{
                    fourDaysText?.visibility = View.GONE
                    divider?.visibility = View.GONE
                }

                if (productDetail?.priceRentalAlt != null){
                    days?.add("7 gün")
                    sevenDaysText?.text = "7 gün: " + productDetail?.priceRentalAlt?.toFloat().toString() + " ₺"
                }else{
                    sevenDaysText?.visibility = View.GONE
                    divider?.visibility = View.GONE
                }

                var adapterDays = ArrayAdapter<String>(applicationContext, R.layout.spinner_item, R.id.spinnerTextView, days!!)

                spinnerDays?.adapter = adapterDays

                spinnerDays?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                        changeSelectedDay = true

                        if (position != 0){

                            selectedDaysName = days?.get(position)

                        }else{

                            selectedDaysName = days?.get(position)

                        }

                    }

                }




                sevenDaysText


            }else{


            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getProductId(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                val productIdData = Gson().fromJson(jsonElement,ProductIdResponse::class.java)

                if (!productIdData.hydraMember.isNullOrEmpty()){


                }


            }else{


            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getOrderDetails(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){




        }else{

        }

    }

    override fun postCheckOrder(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            if (jsonElement != null){

                var data = Gson().fromJson(jsonElement, PostCheckOrderResponse::class.java)

                if (data.response.equals("ok")){

                    orderDetailController?.postOrderDetails(appPreferences.token,"tr_TR",postBody)

                    postBody = JsonObject()

                }else{

                    Toast.makeText(applicationContext,data.response,Toast.LENGTH_LONG).show()

                }

            }else{



            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun postOrderDetails(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            Toast.makeText(applicationContext,"Ürün Sepetinize Başarıyla Eklendi",Toast.LENGTH_LONG).show()

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }


    override fun getCities(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            if (jsonElement != null){

                cityData = Gson().fromJson(jsonElement, CitiesResponse::class.java)

                cityNames = ArrayList()
                cityNames?.add("Şehir")

                for (i in 0..cityData?.hydraMember?.size!!-1){

                    cityNames?.add(cityData?.hydraMember?.get(i)?.name.toString())

                }


                var adapter = ArrayAdapter<String>(applicationContext, R.layout.spinner_item, R.id.spinnerTextView, cityNames!!)

                spinnerCity?.adapter = adapter

                spinnerCity?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                        if (position != 0){

                            selectedCityId = cityData?.hydraMember?.get(position)?.id

                        }else{

                            selectedCityId = null

                        }

                    }

                }


            }else{



            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getCitiesId(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {

    }

    override fun deleteOrderDetailsId(
        response: Boolean,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {

    }

    override fun getProduct(
        response: Boolean?,
        jsonElement: JsonElement?,
        failMessage: Int?,
        error: ErrorResponse?
    ) {
        TODO("Not yet implemented")
    }


}