package com.company.sindirella.response.Product

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ProductDetailResponse {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("notes")
    var notes: String? = null

    @SerializedName("brand")
    var brand: ProductDetailResponseBrand? = null

    @SerializedName("color")
    var color: ProductDetailResponseColor? = null

    @SerializedName("isRental")
    var isRental: Boolean? = null

    @SerializedName("priceRental")
    var priceRental: Int? = null

    @SerializedName("priceSale")
    var priceSale: Int? = null

    @SerializedName("tags")
    var tags: ArrayList<ProductDetailResponseTags>? = null

    @SerializedName("categories")
    var categories: ArrayList<ProductDetailResponseCategories>? = null

    @SerializedName("activities")
    var activities: ArrayList<ProductDetailResponseActivities>? = null

    @SerializedName("pictures")
    var pictures: ArrayList<ProductDetailResponsePictures>? = null

    @SerializedName("inventories")
    var inventories: ArrayList<ProductDetailResponseInventories>? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("fullLink")
    var fullLink: String? = null

    @SerializedName("comments")
    var comments: ArrayList<String>? = null

    @SerializedName("priceRentalAlt")
    var priceRentalAlt: Int? = null

}

class ProductDetailResponseBrand {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("name")
    var name: String? = null

}

class ProductDetailResponseColor {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("name")
    var name: String? = null

}

class ProductDetailResponseTags {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("slug")
    var slug: String? = null

}

class ProductDetailResponseCategories {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

}

class ProductDetailResponseActivities {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

}

class ProductDetailResponsePictures {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("imageUrl")
    var imageUrl: String? = null

    @SerializedName("originSizeUrl")
    var originSizeUrl: String? = null

    @SerializedName("showOrder")
    var showOrder: Int? = null

}

class ProductDetailResponseInventories{

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("size")
    var size: String? = null

    @SerializedName("sizeGroup")
    var sizeGroup: ProductDetailResponseInventoriesSizeGroup? = null

}

class ProductDetailResponseInventoriesSizeGroup {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

}