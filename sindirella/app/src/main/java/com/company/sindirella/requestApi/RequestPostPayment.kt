package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Payment.GetPaymentResponse
import com.company.sindirella.response.Payment.PostPaymentResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class RequestPostPayment(activity: AppCompatActivity?, fragment: Fragment?, token: String?, body: JsonObject?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.PostPayment>(Service.PostPayment::class.java, NetworkSupport.NetworkAdress.base_url)
        request.postPayment(token,body).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}