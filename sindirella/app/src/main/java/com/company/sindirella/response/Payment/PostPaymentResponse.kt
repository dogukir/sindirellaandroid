package com.company.sindirella.response.Payment

import com.google.gson.annotations.SerializedName

class PostPaymentResponse {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("theOrder")
    var theOrder : PostPaymentResponseTheOrder? = null

    @SerializedName("amount")
    var amount : Int? = null

    @SerializedName("url3d")
    var url3d : String? = null

    init {
        id = null
        theOrder = null
        amount = null
    }

}

class PostPaymentResponseTheOrder {

    @SerializedName("transactionId")
    var transactionId : String? = null

}