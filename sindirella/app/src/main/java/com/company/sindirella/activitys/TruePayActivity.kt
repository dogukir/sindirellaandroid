package com.company.sindirella.activitys

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.company.sindirella.R

class TruePayActivity: AppCompatActivity() {


    var closeImage: ImageView? = null
    var transactionIdText: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_true_pay)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)
        transactionIdText = findViewById(R.id.transactionIdText)

        var transactionId = ""
        transactionId = intent.getStringExtra("transactionId")

        if (!transactionId.equals("")){

            transactionIdText?.text = "#${transactionId} numaralı siparişim için ödeme gönderiyorum."

        }


        closeImage?.setOnClickListener {

            var intent = Intent(applicationContext,MainActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()

        }





        setUI()


    }

    @TargetApi(Build.VERSION_CODES.M)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }

    override fun onBackPressed() {
        super.onBackPressed()

        var intent = Intent(applicationContext,MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()

    }

}