package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestGetPayment
import com.company.sindirella.requestApi.RequestPostPayment
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class PaymentController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var paymentListener: PaymentListener? = null

    fun getPayment(page: Int?){
        RequestGetPayment(activity,fragment,page,object : NetworkResponseListener<JsonArray>{
            override fun onResponseReceived(response: JsonArray) {
                paymentListener?.getPayment(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonArray?) {
                paymentListener?.getPayment(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                paymentListener?.getPayment(false,null,failMessage,error)
            }

        })
    }


    fun postPayment(token: String?,body: JsonObject?){
        RequestPostPayment(activity,fragment,token,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                paymentListener?.postPayment(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                paymentListener?.postPayment(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                paymentListener?.postPayment(false,null,failMessage,error)
            }

        })
    }


    interface PaymentListener{
        fun getPayment(response: Boolean?,jsonArray: JsonArray?,failMessage: Int?, error: ErrorResponse?)
        fun postPayment(response: Boolean,jsonElement: JsonElement?,failMessage: Int?, error: ErrorResponse?)
    }



}