package com.company.sindirella.activitys

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.FavProductController
import com.company.sindirella.controller.ProductController
import com.company.sindirella.helpers.BrowseFragmentRecycleAdapter
import com.company.sindirella.helpers.FavActivityRecyclerAdapter
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.Product.ProductResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class FavActivity : AppCompatActivity(), ProductController.ProductListener, FavProductController.FavProductListener, FavActivityRecyclerAdapter.ClickFavActivityRcycleItemListener, FavActivityRecyclerAdapter.FavActivityRecycleAdapterClickListener {

    private var productController: ProductController? = null
    private var productData: ProductResponse? = null
    private var favProductController: FavProductController? = null
    private var favData: GetFavProductResponse? = null
    private var favIdArray: ArrayList<Int>? = null
    private var closeImage: ImageView? = null

    private var adapter : FavActivityRecyclerAdapter? = null

    private var recycler: RecyclerView? = null





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fav)
        supportActionBar?.hide()

        closeImage = findViewById(R.id.closeImage)

        recycler = findViewById(R.id.recycler)

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        recycler?.layoutManager = layoutManager
        recycler?.itemAnimator = DefaultItemAnimator()


        favProductController = FavProductController(this,null)
        favProductController?.favProductListener = this
        favProductController?.getFavProduct(appPreferences.token,null)


        productController = ProductController(this,null)
        productController?.productListener = this


        closeImage?.setOnClickListener {

            onBackPressed()

        }

        setUI()

    }

    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

    }


    override fun getFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            favIdArray = ArrayList()

            favData = Gson().fromJson(jsonElement,GetFavProductResponse::class.java)

            if (!favData?.hydraMember.isNullOrEmpty()){

                for (i in 0 until favData?.hydraMember?.size!!){

                    favIdArray?.add(favData?.hydraMember?.get(i)?.product?.id!!)

                }

                productController?.product(null,favIdArray,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",null,null,null,null)


            }else{

                adapter = FavActivityRecyclerAdapter(this,null,null)
                adapter?.clickFavActivityRcycleItemListener = this
                adapter?.favActivityRecycleAdapterClickListener = this
                recycler?.adapter = adapter

            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun postFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){
            //postFavBool = true

            /*val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()

             */


        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun deleteFavProductId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            //postFavBool = true

            //favProductController?.getFavProduct(appPreferences.token,null)

            val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()



        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

            //postFavBool = true

            /*val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()

             */



            //favProductController?.getFavProduct(appPreferences.token,null)

        }

        //favProductController?.getFavProduct(appPreferences.token,null)

    }



    override fun getProduct(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            productData = Gson().fromJson(jsonElement, ProductResponse::class.java)


            adapter = FavActivityRecyclerAdapter(this,productData,favData)
            adapter?.clickFavActivityRcycleItemListener = this
            adapter?.favActivityRecycleAdapterClickListener = this
            recycler?.adapter = adapter
            recycler?.itemAnimator = DefaultItemAnimator()
            recycler?.layoutManager = GridLayoutManager(this,2)



        }else{

            Toast.makeText(applicationContext,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getProductId(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

    }

    override fun clickFavActivityRcycleItemListener(BigId: String) {

        onBackPressed()

        val intent = Intent(this, ProductDetailActivity::class.java)
        intent.putExtra("id",BigId)
        startActivity(intent)

    }

    override fun likeClickFavActivity(id: Int?, position: Int?) {

        var likeBody = JsonObject()

        likeBody.addProperty("product","/api/products/${id}")
        likeBody.addProperty("status",1)

        favProductController?.postFavProduct(appPreferences.token,likeBody)

    }

    override fun disLikeClickFavActivity(id: Int?, position: Int?) {

        favProductController?.deleteFavProductId(appPreferences.token,id.toString())

    }

}