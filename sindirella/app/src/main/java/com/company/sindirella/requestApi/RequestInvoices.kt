package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Invoice.InvoiceResponse
import com.company.sindirella.response.InvoiceLine.InvoiceLineIdResponse
import java.util.ArrayList

class RequestInvoices(activity: AppCompatActivity?, fragment: Fragment?, page : Int?, listener: NetworkResponseListener<ArrayList<InvoiceResponse>>) {

    init {
        val request = RequestCreator.create<Service.Invoices>(Service.Invoices::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getInvoices(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}