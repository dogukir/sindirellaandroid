package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Product.ProductIdResponse
import com.company.sindirella.response.Product.ProductResponse
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestProductId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.ProductId>(Service.ProductId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getProductId(id).enqueue(NetworkResponse(listener,1,activity,fragment)
        )
    }

}