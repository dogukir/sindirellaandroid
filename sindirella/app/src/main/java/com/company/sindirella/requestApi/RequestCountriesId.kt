package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Country.CountriesIdResponse
import com.company.sindirella.response.Country.CountriesResponse
import java.util.ArrayList

class RequestCountriesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<CountriesIdResponse>) {

    init {
        val request = RequestCreator.create<Service.CountriesId>(Service.CountriesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCountriesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}