package com.company.sindirella.response.Size

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class SizeResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<SizeResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class SizeResponseHydraMember {

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    var name : String? = null

    init {
        id = null
        name = null
    }

}