package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Order.PutOrdersIdResponse
import com.google.gson.JsonElement

class RequestDeleteOrderDetailId(activity: AppCompatActivity?, fragment: Fragment?,token: String?, id: String?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.DeleteOrderDetailsId>(Service.DeleteOrderDetailsId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.deleteOrderDetailsId(token,id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}