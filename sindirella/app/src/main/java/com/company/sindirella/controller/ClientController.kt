package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.*
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonElement
import com.google.gson.JsonObject

class ClientController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var googleListener: GoogleListener? = null
    var passwordListener: PasswordListener? = null
    var meListener: MeListener? = null
    var postLoginListener: onPostLoginListener? = null
    var postRegisterListener: onPostRegisterListener? = null

    fun postlogin(body: JsonObject?){
        RequestLogin(activity,fragment,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                postLoginListener?.postLoginListener(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                postLoginListener?.postLoginListener(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                postLoginListener?.postLoginListener(false,null,failMessage,error)
            }

        })
    }


    fun postRegister(body: JsonObject?){

        RequestRegister(activity, fragment, body, object : NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                postRegisterListener?.onPostRegisterListener(true, response, -1, null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                postRegisterListener?.onPostRegisterListener(true, null, -1, null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                postRegisterListener?.onPostRegisterListener(false, null, failMessage, error)
            }

        })

    }



    fun getMe(token: String?){
        RequestGetMe(activity,fragment,token,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                meListener?.getMe(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                meListener?.getMe(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                meListener?.getMe(false,null,failMessage,error)
            }

        })
    }


    fun putMe(token: String?,body: JsonObject?){
        RequestPutMe(activity,fragment,token,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                meListener?.putMe(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                meListener?.putMe(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                meListener?.putMe(false,null,failMessage,error)
            }

        })
    }


    fun putPassword(token: String?,id: String?,body: JsonObject?){
        RequestPutPassword(activity,fragment,token,id,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                passwordListener?.putPassword(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                passwordListener?.putPassword(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                passwordListener?.putPassword(false,null,failMessage,error)
            }

        })
    }


    fun google(body: JsonObject?){
        RequestGoogle(activity,fragment,body,object: NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                googleListener?.getGoogle(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                googleListener?.getGoogle(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                googleListener?.getGoogle(false,null,failMessage,error)
            }

        })
    }

    interface GoogleListener{
        fun getGoogle(response: Boolean?,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

    interface PasswordListener{
        fun putPassword(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

    interface MeListener{
        fun getMe(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun putMe(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

    interface onPostLoginListener{
        fun postLoginListener(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }

    interface onPostRegisterListener {
        fun onPostRegisterListener(responseOk:Boolean,jsonElement: JsonElement?, failMessage: Int, error: ErrorResponse?)
    }


}