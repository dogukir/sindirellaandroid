package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.HelpPage.HelpPageIdResponse
import com.company.sindirella.response.HelpPage.HelpPageResponse
import java.util.ArrayList

class RequestHelpPagesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<HelpPageIdResponse>) {

    init {
        val request = RequestCreator.create<Service.HelpPagesId>(Service.HelpPagesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getHelpPagesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}