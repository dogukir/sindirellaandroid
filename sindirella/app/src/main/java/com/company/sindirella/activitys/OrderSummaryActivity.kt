package com.company.sindirella.activitys

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.OrderController
import com.company.sindirella.helpers.MyOrdersRecyclerAdapter
import com.company.sindirella.helpers.OrderSummaryViewPagerAdapter
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.Order.ActiveOrdersResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import top.defaults.drawabletoolbox.DrawableBuilder

class OrderSummaryActivity : AppCompatActivity(),OrderController.ActiveOrdersListener {

    private var closeImage: ImageView? = null
    private var transactionIdText: TextView? = null
    private var dateText: TextView? = null
    private var viewPager: ViewPager? = null
    private var totalAmountText: TextView? = null
    private var paymentStrText: TextView? = null
    private var cargoTrackingButton: Button? = null
    private var extraditionRequestButton: Button? = null
    private var productReportButton: Button? = null
    private var addressText: TextView? = null
    private var namePhoneText: TextView? = null
    private var invoiceAddressText: TextView? = null
    private var invoiceNamePhoneText: TextView? = null
    private var paymentType: TextView? = null
    private var cargoText: TextView? = null
    private var totallyAmountText: TextView? = null
    private var salesContractButton: Button? = null
    private var returnConditionsButton: Button? = null
    private var payConfirmText: TextView? = null


    private var orderController: OrderController? = null


    private var orderSummaryViewPagerAdapter: OrderSummaryViewPagerAdapter? = null


    private var orderId: Int? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_summary)
        supportActionBar?.hide()

        orderId = intent.getIntExtra("id",-1)

        closeImage = findViewById(R.id.closeImage)
        transactionIdText = findViewById(R.id.transactionIdText)
        dateText = findViewById(R.id.dateText)
        viewPager = findViewById(R.id.viewPager)
        totalAmountText = findViewById(R.id.totalAmountText)
        paymentStrText = findViewById(R.id.paymentStrText)
        cargoTrackingButton = findViewById(R.id.cargoTrackingButton)
        extraditionRequestButton = findViewById(R.id.extraditionRequestButton)
        productReportButton = findViewById(R.id.productReportButton)
        addressText = findViewById(R.id.addressText)
        namePhoneText = findViewById(R.id.namePhoneText)
        invoiceAddressText = findViewById(R.id.invoiceAddressText)
        invoiceNamePhoneText = findViewById(R.id.invoiceNamePhoneText)
        paymentType = findViewById(R.id.paymentType)
        cargoText = findViewById(R.id.cargoText)
        totallyAmountText = findViewById(R.id.totallyAmountText)
        salesContractButton = findViewById(R.id.salesContractButton)
        returnConditionsButton = findViewById(R.id.returnConditionsButton)
        payConfirmText = findViewById(R.id.payConfirmText)






        orderController = OrderController(this,null)
        orderController?.activeOrdersListener = this
        orderController?.getActiveOrders(appPreferences.token,null,null,null,null,null,null)





        salesContractButton?.setOnClickListener {

            val intent = Intent(applicationContext,MenuWebViewActivity::class.java)
            intent.putExtra("code","satis-sozlesmesi")
            startActivity(intent)
        }

        returnConditionsButton?.setOnClickListener {

            val intent = Intent(applicationContext,MenuWebViewActivity::class.java)
            intent.putExtra("code","cayma-hakki")
            startActivity(intent)

        }


        closeImage?.setOnClickListener {

            onBackPressed()

        }



        setUI()

    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setUI(){

        window.statusBarColor = resources.getColor(R.color.how_working_status_bar)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        cargoTrackingButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(30).build()

        extraditionRequestButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(30).build()

        productReportButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(30).build()

        salesContractButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(30).build()

        returnConditionsButton?.background = DrawableBuilder().solidColor(resources.getColor(R.color.transparent)).strokeColor(resources.getColor(R.color.black)).strokeWidth(3).cornerRadius(30).build()

    }

    override fun getActiveOrders(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            var activeOrdersData = Gson().fromJson(jsonElement, ActiveOrdersResponse::class.java)

            if (!activeOrdersData.hydraMember.isNullOrEmpty()){

                for (i in 0..activeOrdersData.hydraMember?.size!!-1){

                    if (orderId == activeOrdersData.hydraMember?.get(i)?.id){

                        if (!activeOrdersData.hydraMember?.get(i)?.orderDetails.isNullOrEmpty()){

                            orderSummaryViewPagerAdapter = OrderSummaryViewPagerAdapter(applicationContext,activeOrdersData.hydraMember?.get(i)?.orderDetails!!)

                            viewPager?.adapter = orderSummaryViewPagerAdapter

                        }

                        transactionIdText?.text = "#" + activeOrdersData.hydraMember?.get(i)?.transactionId
                        dateText?.text = activeOrdersData.hydraMember?.get(i)?.orderedAtStr
                        totalAmountText?.text = activeOrdersData.hydraMember?.get(i)?.amountNet?.toFloat().toString() + " ₺"
                        payConfirmText?.text = activeOrdersData.hydraMember?.get(i)?.statusStr
                        paymentStrText?.text = activeOrdersData.hydraMember?.get(i)?.paymentType

                        if (activeOrdersData.hydraMember?.get(i)?.paymentType.equals("EFT/Havale")){

                            cargoTrackingButton?.visibility = View.GONE
                            extraditionRequestButton?.visibility = View.GONE
                            productReportButton?.visibility = View.GONE

                        }

                        paymentType?.text = activeOrdersData.hydraMember?.get(i)?.paymentType

                        
                        addressText?.text = activeOrdersData.hydraMember?.get(i)?.shipmentAddress?.address
                        namePhoneText?.text = activeOrdersData.hydraMember?.get(i)?.shipmentAddress?.receiverName + "-" + activeOrdersData.hydraMember?.get(i)?.shipmentAddress?.phone
                        invoiceAddressText?.text = activeOrdersData.hydraMember?.get(i)?.invoiceAddress?.address
                        invoiceNamePhoneText?.text = activeOrdersData.hydraMember?.get(i)?.invoiceAddress?.receiverName + "-" + activeOrdersData.hydraMember?.get(i)?.invoiceAddress?.phone
                        cargoText?.text = activeOrdersData.hydraMember?.get(i)?.amountShipment?.toFloat().toString() + " ₺"
                        totallyAmountText?.text = activeOrdersData.hydraMember?.get(i)?.amountTotal?.toFloat().toString() + " ₺"

                    }

                }

            }

        }else{

            Toast.makeText(applicationContext,error?.hydraDescription, Toast.LENGTH_LONG).show()

        }

    }


}