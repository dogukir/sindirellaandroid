package com.company.sindirella.controller

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.NetworkResponseListener
import com.company.sindirella.requestApi.RequestCheckOrder
import com.company.sindirella.requestApi.RequestDeleteOrderDetailId
import com.company.sindirella.requestApi.RequestGetOrderDetails
import com.company.sindirella.requestApi.RequestPostOrderDetails
import com.company.sindirella.response.ErrorResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.ArrayList

class OrderDetailController(private var activity: AppCompatActivity?, private var fragment: Fragment?) {

    var orderDetailsListener: OrderDetailsListener? = null

    fun getOrderDetails(token: String?,id: Int?, idArray: ArrayList<Int>?, status: Int?, statusArray: ArrayList<Int>?, orderId: String?, page: Int?){
        RequestGetOrderDetails(activity,fragment,token,id,idArray,status,statusArray,orderId,page,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                orderDetailsListener?.getOrderDetails(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                orderDetailsListener?.getOrderDetails(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                orderDetailsListener?.getOrderDetails(false,null,failMessage,error)
            }

        })
    }



    fun postOrderDetails(token: String?,language: String?,body: JsonObject?){
        RequestPostOrderDetails(activity,fragment,token,language,body,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                orderDetailsListener?.postOrderDetails(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                orderDetailsListener?.postOrderDetails(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                orderDetailsListener?.postOrderDetails(false,null,failMessage,error)
            }

        })
    }

    fun postCheckOrder(token: String?,body: JsonObject?){
        RequestCheckOrder(activity,fragment,token,body, object : NetworkResponseListener<JsonElement> {
            override fun onResponseReceived(response: JsonElement) {
                orderDetailsListener?.postCheckOrder(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                orderDetailsListener?.postCheckOrder(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                orderDetailsListener?.postCheckOrder(false,null,-1,error)
            }


        })
    }

    fun deleteOrderDetailId(token: String?,id: String?){
        RequestDeleteOrderDetailId(activity,fragment,token,id,object : NetworkResponseListener<JsonElement>{
            override fun onResponseReceived(response: JsonElement) {
                orderDetailsListener?.deleteOrderDetailsId(true,response,-1,null)
            }

            override fun onEmptyResponse(response: JsonElement?) {
                orderDetailsListener?.deleteOrderDetailsId(true,null,-1,null)
            }

            override fun onError(failMessage: Int, error: ErrorResponse?) {
                orderDetailsListener?.deleteOrderDetailsId(false,null,-1,error)
            }

        })
    }


    interface OrderDetailsListener{
        fun getOrderDetails(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun postOrderDetails(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun postCheckOrder(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
        fun deleteOrderDetailsId(response: Boolean,jsonElement: JsonElement?,failMessage: Int?,error: ErrorResponse?)
    }


}