package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Brand.BrandsIdNameResponse
import com.company.sindirella.response.Category.CategoriesResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestCategories(activity: AppCompatActivity?, fragment: Fragment?, exist: Boolean?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.Categories>(Service.Categories::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getCategories(exist,page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}