package com.company.sindirella.response.SizeGroup

import com.company.sindirella.response.Invertory.ClientInvertory
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class SizeGroupResponse {

    @SerializedName("hydra:member")
    var hydraMember: ArrayList<SizeGroupResponseHydraMember>? = null

    @SerializedName("hydra:totalItems")
    var hydraTotalItems: Int? = null

}

class SizeGroupResponseHydraMember {

    @SerializedName("@id")
    var BigId: String? = null

    @SerializedName("@type")
    var BigType: String? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("definition")
    var definition: String? = null

    @SerializedName("sizes")
    var sizes: ArrayList<String>? = null

    @SerializedName("client")
    var client: ClientInvertory? = null

    @SerializedName("shoulder")
    var shoulder: Int? = null

    @SerializedName("waist")
    var waist: Int? = null

    @SerializedName("chest")
    var chest: Int? = null

    @SerializedName("hip")
    var hip: Int? = null

    @SerializedName("createdBy")
    var createdBy: String? = null

    @SerializedName("updatedBy")
    var updatedBy: String? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("deletedAt")
    var deletedAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    init {
        BigId = null
        BigType = null
        id = null
        name = null
        definition = null
        sizes = null
        client = null
        shoulder = null
        waist = null
        chest = null
        hip = null
        createdBy = null
        updatedBy = null
        createdAt = null
        updatedAt = null
        deletedAt = null
        deleted = null
    }

}