package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.Size.SizeIdResponse
import com.company.sindirella.response.Size.SizeResponse
import java.util.ArrayList

class RequestSizesId(activity: AppCompatActivity?, fragment: Fragment?, id: String?, listener: NetworkResponseListener<SizeIdResponse>) {

    init {
        val request = RequestCreator.create<Service.SizesId>(Service.SizesId::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getSizesId(id).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}