package com.company.sindirella.response.PromotionArea

import com.google.gson.annotations.SerializedName

class PromotionAreaIdResponse {

    @SerializedName("code")
    var code: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("buttonTitle")
    var buttonTitle: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("fullImage")
    var fullImage: String? = null

    @SerializedName("imageUrl")
    var imageUrl: String? = null

    @SerializedName("jsUrl")
    var jsUrl: String? = null

    @SerializedName("apiUrl")
    var apiUrl: String? = null

    init {
        code = null
        url = null
        buttonTitle = null
        title = null
        summary = null
        fullImage = null
        imageUrl = null
        jsUrl = null
        apiUrl = null
    }

}