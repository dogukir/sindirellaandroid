package com.company.sindirella.helpers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.company.sindirella.R
import com.company.sindirella.response.Product.ProductDetailResponse
import com.company.sindirella.response.Product.ProductDetailResponsePictures
import com.company.sindirella.response.Product.ProductIdResponseHydraMemberPictures
import java.util.ArrayList
import top.defaults.drawabletoolbox.DrawableBuilder

class ProductDetailViewPagerAdapter(private var context: Context, private var data: ArrayList<ProductDetailResponsePictures>?) : PagerAdapter() {


    private var imageViewClothes : ImageView? = null
    private var mainLayout : LinearLayout? = null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val view = LayoutInflater.from(context)
        val layout = view.inflate(R.layout.product_detail_item_view,container,false)

        imageViewClothes = layout.findViewById(R.id.imageViewClothes)
        mainLayout = layout.findViewById(R.id.mainLayout)


        mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white)).cornerRadius(50).build()

        val requestOptions = RequestOptions()
        requestOptions.transform(CenterCrop(),GranularRoundedCorners(30f,30f,30f,30f))

        Glide.with(context).load(data?.get(position)?.imageUrl).apply(requestOptions).into(imageViewClothes!!)



        container.addView(layout)


        return layout
    }



    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {

        return data?.size!!
    }

}