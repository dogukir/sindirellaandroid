package com.company.sindirella.requestApi

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.company.sindirella.network.*
import com.company.sindirella.response.PromotionArea.PromotionAreaIdResponse
import com.company.sindirella.response.SizeGroup.SizeGroupResponse
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import java.util.ArrayList

class RequestSizeGroup(activity: AppCompatActivity?, fragment: Fragment?, page: Int?, listener: NetworkResponseListener<JsonElement>) {

    init {
        val request = RequestCreator.create<Service.SizeGroup>(Service.SizeGroup::class.java, NetworkSupport.NetworkAdress.base_url)
        request.getSizeGroup(page).enqueue(NetworkResponse(listener,1,activity,fragment))
    }

}