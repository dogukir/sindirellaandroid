package com.company.sindirella.fragments

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.company.sindirella.R
import com.company.sindirella.activitys.FilterActivity
import com.company.sindirella.activitys.MainActivity
import com.company.sindirella.activitys.ProductDetailActivity
import com.company.sindirella.appPreferences
import com.company.sindirella.controller.DynamicApiController
import com.company.sindirella.controller.FavProductController
import com.company.sindirella.controller.ProductController
import com.company.sindirella.helpers.*
import com.company.sindirella.isScrolledToBottom
import com.company.sindirella.isScrolledToRight
import com.company.sindirella.response.ErrorResponse
import com.company.sindirella.response.FavProduct.GetFavProductResponse
import com.company.sindirella.response.Product.ProductResponse
import com.company.sindirella.view.SortPopUp
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import top.defaults.drawabletoolbox.DrawableBuilder
import java.util.ArrayList

class BrowseFragment(var activity: MainActivity?) : Fragment(),DynamicApiController.DynamicApiListener,SortPopUp.SortClickListener,BrowseFragmentRecycleAdapter.ClickRcycleItemListener,BrowserFragmentViewPagerAdapter.ClickViewPagerItemListener, FavProductController.FavProductListener, BrowseFragmentRecycleAdapter.BrowseRecycleAdapterClickListener, BrowserFragmentViewPagerAdapter.BrowseViewPagerAdapterClickListener, ProductController.ProductListener {


    private var titleText : TextView? = null
    private var totalProduct : TextView? = null
    private var filterText : TextView? = null
    private var sortText : TextView? = null
    private var recycler : RecyclerView? = null
    private var adapter : BrowseFragmentRecycleAdapter? = null
    private var adapterViewPager : BrowserFragmentViewPagerAdapter? = null
    private var viewFilter : View? = null
    private var viewSort : View? = null
    private var displayChangeImage : ImageView? = null
    private var clothesViewPager: ViewPager? = null
    private var favData: GetFavProductResponse? = null

    var sortData = ""


    private var postFavBool = false
    private var likePosition: Int? = null


    var dialogView : SortPopUp? = null

    var productData : ProductResponse? = null
    var productViewPagerSize = 0

    private var productController: ProductController? = null
    private var dynamicApiController: DynamicApiController? = null
    private var favProductController: FavProductController? = null


    private var layoutManager: LinearLayoutManager? = null
    private var gridLayoutManager: GridLayoutManager? = null

    private var productPage = 1
    private var totallyMember = 0
    private var isLastPaged = false
    private var isLoad = false



    var promotionClick: String? = null
    var activityId: Int? = null
    var startCalendar: String? = null
    var endCalendar: String? = null
    var orderPopuSort : String? = null
    var orderIdSort: String? = null
    var orderPriceRentalSort: String? = null


    override fun onResume() {
        super.onResume()

        if (GlobalData.filterClick == true){

            GlobalData.selectedMinAmount
            GlobalData.selectedMaxAmount
            GlobalData.selectedCategoryId
            GlobalData.selectedSizeId
            GlobalData.selectedColorId
            GlobalData.selectedBrandId
            GlobalData.selectedStartDate
            GlobalData.selectedEndDate
            GlobalData.selectedEventsId

            startCalendar = GlobalData.selectedStartDate
            endCalendar = GlobalData.selectedEndDate

            activityId = null

            getProductFunc()

        }else{

            if (GlobalData.filterClick == null){

                //clearParameter()

            }

        }


    }

    override fun onStop() {
        super.onStop()

        println("durdu langgkk")

        GlobalData.filterClick = false
        //productViewPagerSize = 0


        GlobalData.selectedMinAmount = null
        GlobalData.selectedMaxAmount = null
        GlobalData.selectedCategoryId = null
        GlobalData.selectedSizeId = null
        GlobalData.selectedColorId = null
        GlobalData.selectedBrandId = null
        GlobalData.selectedStartDate = null
        GlobalData.selectedEndDate = null
        GlobalData.selectedEventsId = null

    }





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_browse_two,container,false)

        appPreferences.browseGridOrRecycle = true

        promotionClick = this.arguments?.getString("promotionClick","")
        activityId = this.arguments?.getInt("activityId")
        startCalendar = this.arguments?.getString("startCalendar")
        endCalendar = this.arguments?.getString("endCalendar")

        orderPopuSort = "ASC"


        dynamicApiController = DynamicApiController(null,this)
        dynamicApiController?.dynamicApiListener = this

        favProductController = FavProductController(null,this)
        favProductController?.favProductListener = this

        productController = ProductController(null,this)
        productController?.productListener = this

        if (!appPreferences.token.equals("")){
            favProductController?.getFavProduct(appPreferences.token,null)
        }else{

            if (!promotionClick.equals("") && promotionClick != null){

                dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

            }else{

                getProductFunc()

            }


            /*
            if (GlobalData.browse_clothes.equals("")){

                if (GlobalData.productDetailActivitiesId != -1){

                    productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null)

                }else{

                    productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null)

                }

            }else{

                if (GlobalData.productDetailActivitiesId != -1){

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString(),productPage)

                }else{

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null,productPage)

                }

            }

             */

        }




        //productController = ProductController(null,this)
        //productController?.productListener = this
        //productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",null)



        recycler = view.findViewById(R.id.recycler)
        viewFilter = view.findViewById(R.id.viewFilter)
        viewSort = view.findViewById(R.id.viewSort)
        sortText = view.findViewById(R.id.sortText)
        filterText = view.findViewById(R.id.filterText)
        displayChangeImage = view.findViewById(R.id.displayChangeImage)
        totalProduct = view.findViewById(R.id.totalProduct)
        clothesViewPager = view.findViewById(R.id.clothesViewPager)


        //layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false)  //fragment içinte tanımlanan
        //recycler?.layoutManager = layoutManager


        gridLayoutManager = GridLayoutManager(getActivity(),2,GridLayoutManager.VERTICAL,false)
        recycler?.layoutManager = gridLayoutManager
        recycler?.itemAnimator = DefaultItemAnimator()


        displayChangeImage?.setOnClickListener {

            productPage = 1
            productViewPagerSize = 0

            if (!appPreferences.browseGridOrRecycle){
                //adapter = BrowseFragmentRecycleAdapter(context)
                recycler?.visibility = View.VISIBLE
                clothesViewPager?.visibility = View.GONE

                appPreferences.browseGridOrRecycle = true

                if (!promotionClick.equals("") && promotionClick != null){

                    dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                }else{

                    getProductFunc()

                }

                /*

                if (GlobalData.browse_clothes.equals("")){

                    if (GlobalData.productDetailActivitiesId != -1){

                        productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                    }else{

                        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                    }

                }else{

                    if (GlobalData.productDetailActivitiesId != -1){

                        dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString(),productPage)

                    }else{

                        dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null,productPage)

                    }

                }

                 */

                /*adapter = BrowseFragmentRecycleAdapter(context,productData?.hydraMember!!,favData)
                adapter?.clickRcycleItemListener = this
                adapter?.browseRecycleAdapterClickListener = this
                adapter?.addAll(productData?.hydraMember!!)
                recycler?.adapter = adapter
                recycler?.itemAnimator = DefaultItemAnimator()
                recycler?.layoutManager = GridLayoutManager(context,2)

                 */

            }else{
                //adapter = BrowseFragmentRecycleAdapter(context)
                recycler?.visibility = View.GONE
                clothesViewPager?.visibility = View.VISIBLE

                appPreferences.browseGridOrRecycle = false

                if (!promotionClick.equals("") && promotionClick != null){

                    dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                }else{

                    getProductFunc()

                }

                /*
                if (GlobalData.browse_clothes.equals("")){

                    if (GlobalData.productDetailActivitiesId != -1){

                        productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                    }else{

                        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                    }

                }else{

                    if (GlobalData.productDetailActivitiesId != -1){

                        dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString(),productPage)

                    }else{

                        dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null,productPage)

                    }

                }

                 */

                /*adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData,favData)
                adapterViewPager?.clickViewPagerItemListener = this
                adapterViewPager?.browseViewPagerAdapterClickListener = this

                clothesViewPager?.adapter = adapterViewPager

                clothesViewPager?.pageMargin = 30
                clothesViewPager?.setPadding(80,0,80,0)
                clothesViewPager?.clipToPadding = false
                clothesViewPager?.offscreenPageLimit = 0


                 */
            }


        }


        viewSort?.setOnClickListener {

            var arr = ArrayList<String>()

            dialogView = SortPopUp(context!!,arr)
            dialogView?.sortClickListener = this
            dialogView?.createView(R.layout.sort_pop_up)
            dialogView?.show()
            dialogView?.setCanceledOnTouchOutside(false)


            dialogView?.exitButton?.setOnClickListener {

                dialogView?.dismiss()

            }

        }


        viewFilter?.setOnClickListener {

            val intent = Intent(context,FilterActivity::class.java)
            startActivity(intent)

        }




        recycler?.addOnScrollListener(object : PaginationScrollGridListener(gridLayoutManager!!) {

            override var totalPageCount: Int = if(totallyMember != 0) totallyMember else 0

            override var isLastPage: Boolean = isLastPaged

            override var isLoading: Boolean =  isLoad

            override fun loadMoreItems() {
                if(isLoading) return
                if (recycler?.isScrolledToBottom()!!) {

                    /*var handler = Handler().post {
                        activity.showLoading()
                    }*/

                    isLoad = true
                    productPage += 1
                    loadNextPage()
                }
            }

        })

        clothesViewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

                if (position == productViewPagerSize-5){

                    productPage += 1

                    if (!promotionClick.equals("") && promotionClick != null){

                        dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                    }else{

                        getProductFunc()

                    }

                    /*
                    if (GlobalData.browse_clothes.equals("")){

                        if (GlobalData.productDetailActivitiesId != -1){

                            productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                        }else{

                            productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                        }

                    }else{

                        if (GlobalData.productDetailActivitiesId != -1){

                            dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString(),productPage)

                        }else{

                            dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null,productPage)

                        }

                    }

                     */

                }

            }

        })




        setUI()


        return view
    }

    fun getProductFunc(){

        productController?.product(null,null,GlobalData.selectedBrandId.toString(),null,null,GlobalData.selectedColorId,activityId.toString()
            ,GlobalData.selectedEventsId,null,GlobalData.selectedSizeId,GlobalData.selectedCategoryId.toString(),null,null,null
            ,null,null,GlobalData.selectedMinAmount,GlobalData.selectedMaxAmount,null,null,orderPriceRentalSort,orderPopuSort
            ,productPage,orderIdSort,startCalendar,endCalendar)

    }

    fun clearParameter(){

        GlobalData.selectedMinAmount = null
        GlobalData.selectedMaxAmount = null
        GlobalData.selectedCategoryId = null
        GlobalData.selectedSizeId = null
        GlobalData.selectedColorId = null
        GlobalData.selectedBrandId = null
        GlobalData.selectedStartDate = null
        GlobalData.selectedEndDate = null
        GlobalData.selectedEventsId = null
        activityId = null
        orderPriceRentalSort = null
        orderPopuSort = "ASC"
        orderIdSort = null
        startCalendar = null
        endCalendar = null

        getProductFunc()

    }


    override fun onDestroyView() {

        GlobalData.productDetailActivitiesId = -1
        super.onDestroyView()
    }


    fun setUI(){

        viewFilter?.background = DrawableBuilder().strokeColor(resources.getColor(R.color.black)).strokeWidth(4).cornerRadius(20).build()
        viewSort?.background = DrawableBuilder().strokeColor(resources.getColor(R.color.black)).strokeWidth(4).cornerRadius(20).build()


    }

    override fun likeClickBrowse(id: Int?, position: Int?) {

        likePosition = position

        var likeBody = JsonObject()

        likeBody.addProperty("product","/api/products/${id}")
        likeBody.addProperty("status",1)

        favProductController?.postFavProduct(appPreferences.token,likeBody)

    }

    override fun disLikeClickBrowse(id: Int?, position: Int?) {

        favProductController?.deleteFavProductId(appPreferences.token,id.toString())

    }

    override fun likeClickBrowseViewPager(id: Int?, position: Int?) {

        likePosition = position

        var likeBody = JsonObject()

        likeBody.addProperty("product","/api/products/${id}")
        likeBody.addProperty("status",1)

        favProductController?.postFavProduct(appPreferences.token,likeBody)

    }

    override fun disLikeClickBrowseViewPager(id: Int?, position: Int?) {

        favProductController?.deleteFavProductId(appPreferences.token,id.toString())

    }


    override fun getProduct(response: Boolean?, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response == true){

            if (jsonElement != null){

                productData = Gson().fromJson(jsonElement,ProductResponse::class.java)

                if(!productData?.hydraMember.isNullOrEmpty()){

                    if (appPreferences.browseGridOrRecycle){

                        if (productPage == 1){

                            recycler?.visibility = View.VISIBLE
                            clothesViewPager?.visibility = View.GONE

                            totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                            adapter = BrowseFragmentRecycleAdapter(context,productData?.hydraMember!!,favData)
                            adapter?.clickRcycleItemListener = this
                            adapter?.browseRecycleAdapterClickListener = this
                            adapter?.addAll(productData?.hydraMember!!)
                            recycler?.adapter = adapter
                            //recycler?.itemAnimator = DefaultItemAnimator()
                            //recycler?.layoutManager = GridLayoutManager(context,2)

                            totallyMember = productData?.hydraTotalItems!!/20 + 1

                        }else{

                            adapter?.addAll(productData?.hydraMember!!)

                        }

                    }else{

                        productViewPagerSize = productViewPagerSize + productData?.hydraMember?.size!!

                        if (productPage == 1){

                            recycler?.visibility = View.GONE
                            clothesViewPager?.visibility = View.VISIBLE

                            totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                            adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData?.hydraMember!!,favData)
                            adapterViewPager?.clickViewPagerItemListener = this
                            adapterViewPager?.browseViewPagerAdapterClickListener = this
                            adapterViewPager?.addAll(productData?.hydraMember!!)

                            clothesViewPager?.adapter = adapterViewPager

                            clothesViewPager?.pageMargin = 20
                            clothesViewPager?.setPadding(40,0,40,0)
                            clothesViewPager?.clipToPadding = false
                            clothesViewPager?.offscreenPageLimit = 0

                            totallyMember = productData?.hydraTotalItems!!/20 + 1

                        }else{

                            adapterViewPager?.addAll(productData?.hydraMember!!)

                        }

                        /*recycler?.visibility = View.GONE
                        clothesViewPager?.visibility = View.VISIBLE

                        totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                        adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData,favData)
                        adapterViewPager?.clickViewPagerItemListener = this
                        adapterViewPager?.browseViewPagerAdapterClickListener = this

                        clothesViewPager?.adapter = adapterViewPager

                        clothesViewPager?.pageMargin = 30
                        clothesViewPager?.setPadding(80,0,80,0)
                        clothesViewPager?.clipToPadding = false
                        clothesViewPager?.offscreenPageLimit = 0

                         */

                    }


                    //recycler?.adapter = adapter

                }else{

                    Toast.makeText(context,"Ürün Bulunamamıştır.",Toast.LENGTH_LONG).show()
                    clearParameter()

                }

            }

        }else{

            Toast.makeText(context,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun getProductId(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

    }



    override fun getFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            if (postFavBool == false){

                favData = Gson().fromJson(jsonElement,GetFavProductResponse::class.java)

                if (!promotionClick.equals("") && promotionClick != null){

                    dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                }else{

                    getProductFunc()

                }

                /*
                if (GlobalData.browse_clothes.equals("")){

                    if (GlobalData.productDetailActivitiesId != -1){

                        productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                    }else{

                        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                    }

                }else{

                    if (GlobalData.productDetailActivitiesId != -1){

                        dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString(),productPage)

                    }else{

                        dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null,productPage)

                    }

                }

                 */

            }else{

                favData = Gson().fromJson(jsonElement,GetFavProductResponse::class.java)

                postFavBool = false

                if (appPreferences.browseGridOrRecycle){
                    adapter?.changeFav(favData!!,likePosition)
                    adapter?.notifyDataSetChanged()
                }else{
                    adapterViewPager?.changeFav(favData!!,likePosition)
                    adapterViewPager?.notifyDataSetChanged()
                }

            }

        }else{

            Toast.makeText(context,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun postFavProduct(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){
            postFavBool = true

            val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()


        }else{

            Toast.makeText(context,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun deleteFavProductId(response: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (response){

            postFavBool = true

            //favProductController?.getFavProduct(appPreferences.token,null)

            val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()



        }else{

            Toast.makeText(context,error?.hydraDescription,Toast.LENGTH_LONG).show()

            /*postFavBool = true

            val count = object: CountDownTimer(300,300){
                override fun onFinish() {
                    favProductController?.getFavProduct(appPreferences.token,null)
                }

                override fun onTick(millisUntilFinished: Long) {

                }

            }
            count.start()

             */

            //favProductController?.getFavProduct(appPreferences.token,null)

        }

    }

    private fun loadNextPage() {

        if (productPage <= totallyMember){

            if (!promotionClick.equals("") && promotionClick != null){

                dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

            }else{

                getProductFunc()

            }

            /*

            if (GlobalData.browse_clothes.equals("")){

                if (GlobalData.productDetailActivitiesId != -1){

                    productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                }else{

                    productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,"ASC",productPage,null,startCalendar,endCalendar)

                }

            }else{

                if (GlobalData.productDetailActivitiesId != -1){

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,GlobalData.productDetailActivitiesId.toString(),productPage)

                }else{

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,"ASC",null,null,null,productPage)

                }

            }

             */

            isLastPaged = false
            isLoad = false

        }

    }

    override fun getDynamicApi(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int?, error: ErrorResponse?) {

        if (responseOk){

            if (jsonElement != null){

                productData = Gson().fromJson(jsonElement,ProductResponse::class.java)

                if(!productData?.hydraMember.isNullOrEmpty()){

                    if (appPreferences.browseGridOrRecycle){



                        if (productPage == 1){

                            recycler?.visibility = View.VISIBLE
                            clothesViewPager?.visibility = View.GONE

                            totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                            adapter = BrowseFragmentRecycleAdapter(context,productData?.hydraMember!!,favData)
                            adapter?.clickRcycleItemListener = this
                            adapter?.browseRecycleAdapterClickListener = this
                            adapter?.addAll(productData?.hydraMember!!)
                            recycler?.adapter = adapter
                            //recycler?.itemAnimator = DefaultItemAnimator()
                            //recycler?.layoutManager = GridLayoutManager(context,2)

                            totallyMember = productData?.hydraTotalItems!!/20 + 1

                        }else{

                            adapter?.addAll(productData?.hydraMember!!)

                        }

                    }else{

                        productViewPagerSize = productViewPagerSize + productData?.hydraMember?.size!!

                        if (productPage == 1){

                            recycler?.visibility = View.GONE
                            clothesViewPager?.visibility = View.VISIBLE

                            totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                            adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData?.hydraMember!!,favData)
                            adapterViewPager?.clickViewPagerItemListener = this
                            adapterViewPager?.browseViewPagerAdapterClickListener = this
                            adapterViewPager?.addAll(productData?.hydraMember!!)

                            clothesViewPager?.adapter = adapterViewPager

                            clothesViewPager?.pageMargin = 20
                            clothesViewPager?.setPadding(40,0,40,0)
                            clothesViewPager?.clipToPadding = false
                            clothesViewPager?.offscreenPageLimit = 0

                            totallyMember = productData?.hydraTotalItems!!/20 + 1

                        }else{

                            adapterViewPager?.addAll(productData?.hydraMember!!)

                        }

                        /*recycler?.visibility = View.GONE
                        clothesViewPager?.visibility = View.VISIBLE

                        totalProduct?.text = "${productData?.hydraTotalItems} ürün"

                        adapterViewPager = BrowserFragmentViewPagerAdapter(context!!,productData,favData)
                        adapterViewPager?.clickViewPagerItemListener = this
                        adapterViewPager?.browseViewPagerAdapterClickListener = this

                        clothesViewPager?.adapter = adapterViewPager

                        clothesViewPager?.pageMargin = 30
                        clothesViewPager?.setPadding(80,0,80,0)
                        clothesViewPager?.clipToPadding = false
                        clothesViewPager?.offscreenPageLimit = 0

                         */

                    }


                    //recycler?.adapter = adapter

                }


            }


        }else{

            Toast.makeText(context,error?.hydraDescription,Toast.LENGTH_LONG).show()

        }

    }

    override fun sortClickPopUp(key: String?, sort: String?) {

        productPage = 1
        productViewPagerSize = 0
        sortData = sort.toString()

        when(key){

            "order[popularity]" -> {

                orderPopuSort = sort
                orderIdSort = null
                orderPriceRentalSort = null


                if (!promotionClick.equals("") && promotionClick != null){

                    dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                }else{

                    getProductFunc()

                }

                /*

                if (GlobalData.browse_clothes.equals("")){

                    if (GlobalData.productDetailActivitiesId != -1){

                        productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,sort,productPage,null,startCalendar,endCalendar)

                    }else{

                        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,sort,productPage,null,startCalendar,endCalendar)

                    }

                }else{

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,sort,null,null,null,productPage)

                }

                 */

            }

            "order[id]" -> {

                orderIdSort = sort
                orderPriceRentalSort = null
                orderPopuSort = null


                if (!promotionClick.equals("") && promotionClick != null){

                    dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                }else{

                    getProductFunc()

                }

                /*

                if (GlobalData.browse_clothes.equals("")){

                    if (GlobalData.productDetailActivitiesId != -1){

                        productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,productPage,sort,startCalendar,endCalendar)

                    }else{

                        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,productPage,sort,startCalendar,endCalendar)

                    }

                }else{

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,null,null,sort,null,productPage)

                }

                 */

            }

            "order[priceRental]" -> {

                orderPriceRentalSort = sort
                orderPopuSort = null
                orderIdSort = null


                if (!promotionClick.equals("") && promotionClick != null){

                    dynamicApiController?.dynamicApi(promotionClick.toString(),orderPopuSort,orderPriceRentalSort,orderIdSort,null,productPage)

                }else{

                    getProductFunc()

                }

                /*
                if (GlobalData.browse_clothes.equals("")){

                    if (GlobalData.productDetailActivitiesId != -1){

                        productController?.product(null,null,null,null,null,null,GlobalData.productDetailActivitiesId.toString(),null,null,null,null,null,null,null,null,null,null,null,null,null,sort,null,productPage,null,startCalendar,endCalendar)

                    }else{

                        productController?.product(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,sort,null,productPage,null,startCalendar,endCalendar)

                    }

                }else{

                    dynamicApiController?.dynamicApi(GlobalData.browse_clothes,null,sort,null,null,productPage)

                }

                 */

            }


        }

        dialogView?.dismiss()

    }

    override fun clickRcycleItemListener(BigId: String) {
        val intent = Intent(context, ProductDetailActivity::class.java)
        intent.putExtra("id",BigId)
        startActivity(intent)
    }

    override fun clickViewPagerItem(BigId: String) {
        val intent = Intent(context, ProductDetailActivity::class.java)
        intent.putExtra("id",BigId)
        startActivity(intent)
    }


}